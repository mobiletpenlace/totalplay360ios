//
//  KeysEnums.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class KeysEnums: NSObject {

}
extension Dictionary {
    
    subscript(key: APIKeys) -> Value? {
        get {
            return self[String(describing: key) as! Key]
        }
        set(value) {
            guard
                let value = value else {
                    self.removeValue(forKey: String(describing: key) as! Key)
                    return
            }
            
            self.updateValue(value, forKey: String(describing: key) as! Key)
        }
    }
    
}

protocol APIKeys {}

enum KeysEnum : APIKeys {
    
    
    case EXTRA_EMPLOYED
    
    
}
