//
//  StringDialogs.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class StringDialogs: NSObject {
    static let dialog_error_intern = "Ocurrio un error al conectar con el servidor"
    static let dialog_error_connection = "Verifique conexión de red"

}
