//
//  SearchAssistancePresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol SearchAssistanceDelegate: NSObjectProtocol {
    func OnSuccessSearchAssistance(searchAssistanceResponse: SearchAssistanceResponse)
}
class SearchAssistancePresenter: BaseTotalplay360Presenter {
    var mSearchAssistanceDelegate: SearchAssistanceDelegate!
    
    func getSearchAssistance(mIdEmployee: String, mStartDate: String, mEndDate: String, mPeriod: String, mSearchAssistanceDelegate: SearchAssistanceDelegate){
        self.mSearchAssistanceDelegate = mSearchAssistanceDelegate
        
        let searchAssistanceRequest = SearchAssistanceRequest()
        
        searchAssistanceRequest.MLogin = Login()
        searchAssistanceRequest.MLogin?.Ip = "10.213.13.60"
        searchAssistanceRequest.MLogin?.User = "1060367"
        searchAssistanceRequest.MLogin?.Password = "Totalplay180x2$"
        
        searchAssistanceRequest.IdEmployee = mIdEmployee
        searchAssistanceRequest.StartDate = mStartDate
        searchAssistanceRequest.EndDate = mEndDate
        searchAssistanceRequest.Period = mPeriod
        
        RetrofitManager<SearchAssistanceResponse>.init(requestUrl: ApiDefinition.WS_SEARCHASSISTANCE, delegate: self).request(requestModel: searchAssistanceRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_SEARCHASSISTANCE){
            AlertDialog.hideOverlay()
            OnSuccessSearchAssistance(searchAssistanceResponse: response as! SearchAssistanceResponse)
        }
    }
    func OnSuccessSearchAssistance(searchAssistanceResponse: SearchAssistanceResponse){
        if searchAssistanceResponse.MResult?.Result == "0" {
            mSearchAssistanceDelegate.OnSuccessSearchAssistance(searchAssistanceResponse: searchAssistanceResponse)
        } else {
            if searchAssistanceResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (searchAssistanceResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
        
    }
}
