//
//  SendAuthorization.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol SendAuthorizationDelegate: NSObjectProtocol {
    func OnSuccessSendAuthorization(sendAuthorizationResponse: SendAuthorizationResponse)
}

class SendAuthorization: BaseTotalplay360Presenter {
    var mSendAuthorizationDelegate: SendAuthorizationDelegate!
    
    func getSendAuthorization(mIdRequested: String, mIdEmployee: String, mStatusRequest: String, mSendAuthorizationDelegate: SendAuthorizationDelegate){
        self.mSendAuthorizationDelegate = mSendAuthorizationDelegate
        
        let sendAuthorizationRequest = SendAuthorizationRequest()
        
        sendAuthorizationRequest.MLogin = Login()
        sendAuthorizationRequest.MLogin?.Ip = "10.213.13.60"
        sendAuthorizationRequest.MLogin?.User = "1060367"
        sendAuthorizationRequest.MLogin?.Password = "Totalplay180x2$"
        
        sendAuthorizationRequest.IdRequested = mIdRequested
        sendAuthorizationRequest.IdEmployee = mIdEmployee
        sendAuthorizationRequest.StatusRequest = mStatusRequest
        
        
        RetrofitManager<SendAuthorizationResponse>.init(requestUrl: ApiDefinition.WS_SENDAUTHORIZATION, delegate: self).request(requestModel: sendAuthorizationRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_SENDAUTHORIZATION){
            AlertDialog.hideOverlay()
            OnSuccessSendAuthorization(sendAuthorizationResponse: response as! SendAuthorizationResponse)
        }
    }
    func OnSuccessSendAuthorization(sendAuthorizationResponse: SendAuthorizationResponse){
        if sendAuthorizationResponse.MResult?.Result == "0" {
            mSendAuthorizationDelegate.OnSuccessSendAuthorization(sendAuthorizationResponse: sendAuthorizationResponse)
        } else {
            if sendAuthorizationResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (sendAuthorizationResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }

}
