//
//  RegisterAssistancePresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol RegisterAssistanceDelegate: NSObjectProtocol {
    func OnSuccessRegisterAssistance(registerAssistanceResponse: RegisterAssistanceResponse)
}
class RegisterAssistancePresenter: BaseTotalplay360Presenter {
    var mRegisterAssistanceDelegate: RegisterAssistanceDelegate!
    
    func getRegisterAssistance(mIdEmployee: String, mTypeAssistance: String, mRegisterAssistanceDelegate: RegisterAssistanceDelegate){
        self.mRegisterAssistanceDelegate = mRegisterAssistanceDelegate
        
        let registerAssistanceRequest = RegisterAssistanceRequest()
        
        registerAssistanceRequest.MLogin = Login()
        registerAssistanceRequest.MLogin?.Ip = "10.213.13.60"
        registerAssistanceRequest.MLogin?.User = "1060367"
        registerAssistanceRequest.MLogin?.Password = "Totalplay180x2$"
        
        registerAssistanceRequest.IdEmployee = mIdEmployee
        registerAssistanceRequest.TypeAssistance = mTypeAssistance
        
        
        RetrofitManager<RegisterAssistanceResponse>.init(requestUrl: ApiDefinition.WS_REGISTERASSISTANCE, delegate: self).request(requestModel: registerAssistanceRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_REGISTERASSISTANCE){
            AlertDialog.hideOverlay()
            OnSuccessRegisterAssistance(registerAssistanceResponse: response as! RegisterAssistanceResponse)
        }
    }
    func OnSuccessRegisterAssistance(registerAssistanceResponse: RegisterAssistanceResponse){
        print(registerAssistanceResponse.MResult?.Result)
        if registerAssistanceResponse.MResult?.Result == "0" {
            mRegisterAssistanceDelegate.OnSuccessRegisterAssistance(registerAssistanceResponse: registerAssistanceResponse)
        } else {
            if registerAssistanceResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (registerAssistanceResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }

}
