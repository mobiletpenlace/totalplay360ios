//
//  VacationStatusPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol VacationStatusDelegate: NSObjectProtocol {
    func OnSuccessVacationStatus(vacationStatusResponse: VacationStatusResponse)
}
class VacationStatusPresenter: BaseTotalplay360Presenter {
    var mVacationStatusDelegate: VacationStatusDelegate!
    
    func getVacationStatus(mIdEmployee: String, mVacationStatusDelegate: VacationStatusDelegate){
        self.mVacationStatusDelegate = mVacationStatusDelegate
        
        let vacationStatusRequest = VacationStatusRequest()
        
        vacationStatusRequest.MLogin = Login()
        vacationStatusRequest.MLogin?.Ip = "10.213.13.60"
        vacationStatusRequest.MLogin?.User = "1060367"
        vacationStatusRequest.MLogin?.Password = "Totalplay180x2$"
        
        vacationStatusRequest.IdEmployee = mIdEmployee
        
        RetrofitManager<VacationStatusResponse>.init(requestUrl: ApiDefinition.WS_VACATIONSTATUS, delegate: self).request(requestModel: vacationStatusRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_VACATIONSTATUS){
            AlertDialog.hideOverlay()
            OnSuccessVacationStatus(vacationStatusResponse: response as! VacationStatusResponse)
        }
    }
    func OnSuccessVacationStatus(vacationStatusResponse: VacationStatusResponse){
        //print(vacationStatusResponse.MResult?.Result)
        if vacationStatusResponse.MResult?.Result == "0" {
            mVacationStatusDelegate.OnSuccessVacationStatus(vacationStatusResponse: vacationStatusResponse)
        } else {
            if vacationStatusResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (vacationStatusResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
        
    }
}
