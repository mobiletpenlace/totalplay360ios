//
//  ConsultaColaboradoresPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
//import ObjectMapper


protocol ConsultaColaboradoresDelegate: NSObjectProtocol {
    func OnSuccessColaboradores(consultaColaboradoresResponse: ConsultaColaboradoresResponse)
}
class ConsultaColaboradoresPresenter: BaseTotalplay360Presenter {
    var mConsultaColaboradoresDelegate: ConsultaColaboradoresDelegate!
    
    func getConsultaColaboradores(mIdEmployee: String, mConsultaColaboradoresDelegate: ConsultaColaboradoresDelegate){
        self.mConsultaColaboradoresDelegate = mConsultaColaboradoresDelegate
        let consultaColaboradoresRequest = ConsultaColaboradoresRequest()
        
        consultaColaboradoresRequest.MLogin = Login()
        consultaColaboradoresRequest.MLogin?.User = "1060367"
        consultaColaboradoresRequest.MLogin?.Password = "Totalplay180x2$"
        consultaColaboradoresRequest.MLogin?.Ip = "10.213.13.60"
        
        consultaColaboradoresRequest.IdEmployee = mIdEmployee
        
        RetrofitManager<ConsultaColaboradoresResponse>.init(requestUrl: ApiDefinition.WS_GETINFOCOLLAB, delegate: self).request(requestModel: consultaColaboradoresRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_GETINFOCOLLAB){
            AlertDialog.hideOverlay()
            OnSuccessColaboradores(consultaColaboradoresRespnse: response as! ConsultaColaboradoresResponse)
        }
    }
    func OnSuccessColaboradores(consultaColaboradoresRespnse: ConsultaColaboradoresResponse){
        if consultaColaboradoresRespnse.MResult?.Result == "0"{
            mConsultaColaboradoresDelegate.OnSuccessColaboradores(consultaColaboradoresResponse: consultaColaboradoresRespnse)
        } else {
            if consultaColaboradoresRespnse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (consultaColaboradoresRespnse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }
    
}
