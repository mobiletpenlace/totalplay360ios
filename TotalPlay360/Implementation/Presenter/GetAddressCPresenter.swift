//
//  GetAddressCPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 22/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

protocol GetAddressCDelegate: NSObjectProtocol {
    func OnSuccessGetAddressC(getAddressCResponse: GetAddressCResponse)
    
}
class GetAddressCPresenter: BaseTotalplay360Presenter {
    var mGetAddressCDelegate: GetAddressCDelegate!
    
    func getAddressC(mGetAddressCDelegate: GetAddressCDelegate){
        self.mGetAddressCDelegate = mGetAddressCDelegate
        let getAddressCRequest = GetAddressCRequest()
        
        getAddressCRequest.MLogin = Login()
        getAddressCRequest.MLogin?.User = "1060367"
        getAddressCRequest.MLogin?.Password = "Totalplay180x2$"
        getAddressCRequest.MLogin?.Ip = "10.213.13.60"
        
        RetrofitManager<GetAddressCResponse>.init(requestUrl: ApiDefinition.WS_GETADDRESSC, delegate: self).request(requestModel: getAddressCRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_GETADDRESSC){
            AlertDialog.hideOverlay()
            OnSuccessGetAddressC(getAddressCResponse: response as! GetAddressCResponse)
        }
    }
    func OnSuccessGetAddressC(getAddressCResponse: GetAddressCResponse){
        if getAddressCResponse.MResult?.Result == "0"{
           mGetAddressCDelegate.OnSuccessGetAddressC(getAddressCResponse: getAddressCResponse)
        } else {
            if getAddressCResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (getAddressCResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }
}
