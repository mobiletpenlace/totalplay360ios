//
//  RequestVacationsPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol RequestVacationsDelegate: NSObjectProtocol {
    func OnSuccessRequestVacations(requestVacationsResponse: RequestVacationsResponse)
}
class RequestVacationsPresenter: BaseTotalplay360Presenter {
    var mRequestVacationsDelegate: RequestVacationsDelegate!
    
    func getRequestVacations(mIdEmployee: String, mTypeRequest: String, mStartDate: String, mEndDate: String, mRequestVacationsDelegate: RequestVacationsDelegate){
        self.mRequestVacationsDelegate = mRequestVacationsDelegate
        
        let requestVacationsRequest = RequestVacationsRequest()
        
        requestVacationsRequest.MLogin = Login()
        requestVacationsRequest.MLogin?.Ip = "10.213.13.60"
        requestVacationsRequest.MLogin?.User = "1060367"
        requestVacationsRequest.MLogin?.Password = "Totalplay180x2$"
        
        requestVacationsRequest.IdEmployee = mIdEmployee
        requestVacationsRequest.TypeRequest = mTypeRequest
        requestVacationsRequest.StartDate = mStartDate
        requestVacationsRequest.EndDate = mEndDate
        
        RetrofitManager<RequestVacationsResponse>.init(requestUrl: ApiDefinition.WS_REQUESTVACATIONS, delegate: self).request(requestModel: requestVacationsRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_REQUESTVACATIONS){
            AlertDialog.hideOverlay()
            OnSuccessRequestVacations(requestVacationsResponse: response as! RequestVacationsResponse)
        }
    }
    func OnSuccessRequestVacations(requestVacationsResponse: RequestVacationsResponse){
        if requestVacationsResponse.MResult?.Result == "0" {
            mRequestVacationsDelegate.OnSuccessRequestVacations(requestVacationsResponse: requestVacationsResponse)
        } else {
            if requestVacationsResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (requestVacationsResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }

}
