//
//  ConsultaEmpleadoPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

protocol ConsultaEmpleadoDelegate: NSObjectProtocol {
    func OnSuccessEmpleado(consultaEmpleadoResponse: ConsultaEmpleadoResponse)
}
class ConsultaEmpleadoPresenter: BaseTotalplay360Presenter {
    var mConsultaEmpleadoDelegate: ConsultaEmpleadoDelegate!
    
    func getConsultaEmpleado(mNumeroEmployeer: String, mConsultaEmpleadoDelegate: ConsultaEmpleadoDelegate){
        self.mConsultaEmpleadoDelegate = mConsultaEmpleadoDelegate
        let consultaEmpleadoRequest = ConsultaEmpleadoRequest()
        
        consultaEmpleadoRequest.MLogin = Login()
        consultaEmpleadoRequest.MLogin?.User = "1060367"
        consultaEmpleadoRequest.MLogin?.Password = "Totalplay180x2$"
        consultaEmpleadoRequest.MLogin?.Ip = "10.213.13.60"
        
        consultaEmpleadoRequest.NumEmployee = mNumeroEmployeer
        
        RetrofitManager<ConsultaEmpleadoResponse>.init(requestUrl: ApiDefinition.WS_GETINFOEMP, delegate: self).request(requestModel: consultaEmpleadoRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_GETINFOEMP){
            AlertDialog.hideOverlay()
            OnSuccessEmpleado(consultaEmpleadoResponse: response as! ConsultaEmpleadoResponse)
        }
        
    }
    func OnSuccessEmpleado (consultaEmpleadoResponse: ConsultaEmpleadoResponse){
        if consultaEmpleadoResponse.MResult?.Result == "0" {
        mConsultaEmpleadoDelegate.OnSuccessEmpleado(consultaEmpleadoResponse: consultaEmpleadoResponse)
  
        } else {
            if consultaEmpleadoResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (consultaEmpleadoResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
            
        }
    }
    
}
