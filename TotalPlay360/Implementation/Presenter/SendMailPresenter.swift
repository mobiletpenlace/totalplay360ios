//
//  SendMailPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol SendMailDelegate: NSObjectProtocol {
    func OnSuccessSendMail(sendMailResponse: SendMailResponse)
}
class SendMailPresenter: BaseTotalplay360Presenter {
    var mSendMailDelegate: SendMailDelegate!
    
    func getSendMail(mto: String, mcc: String, mbcc: String, mreply_to: String, msubject: String, mbody: String, mfrom_Address: String, mfrom_Personal: String, mSendMailDelegate: SendMailDelegate){
        self.mSendMailDelegate = mSendMailDelegate
        
        let sendMailRequest = SendMailRequest()
        
        sendMailRequest.MLogin = Login()
        sendMailRequest.MLogin?.Ip = "10.213.13.60"
        sendMailRequest.MLogin?.User = "1060367"
        sendMailRequest.MLogin?.Password = "Totalplay180x2$"
        
        sendMailRequest.to = mto
        sendMailRequest.cc = mcc
        sendMailRequest.bcc = mbcc
        sendMailRequest.reply_to = mreply_to
        sendMailRequest.subject = msubject
        sendMailRequest.body = mbody
        sendMailRequest.from_Address = mfrom_Address
        sendMailRequest.from_Personal = mfrom_Personal
        
        RetrofitManager<SendMailResponse>.init(requestUrl: ApiDefinition.WS_SENDMAIL, delegate: self).request(requestModel: sendMailRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_SENDMAIL){
            AlertDialog.hideOverlay()
            OnSuccessSendMail(sendMailResponse: response as! SendMailResponse)
        }
    }
    func OnSuccessSendMail(sendMailResponse: SendMailResponse){
        if sendMailResponse.MResult?.Result == "0" {
            mSendMailDelegate.OnSuccessSendMail(sendMailResponse: sendMailResponse)
        } else {
            if sendMailResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (sendMailResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }
}
