//
//  LoginPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift

protocol LoginDelegate: NSObjectProtocol {
    func OnSuccessLogin(mUserId: LoginResponse)
}

class LoginPresenter: BaseTotalplay360Presenter {
    var mLoginDelegate: LoginDelegate!
    var mNumeroEmpleado: String? //*
    var mPasswordEmpleado: String? //*
    var realm = try! Realm()
    let datas = InfoEmployee()
    var nEmpD : String?
    override init(viewController: BaseViewController) {
        super.init(viewController: viewController)
        
        
    }
    func getLogin(mUser: String , mPassword: String, mLoginDelegate: LoginDelegate){
        
        self.mLoginDelegate = mLoginDelegate
        let loginRequest = LoginRequest()
        
        loginRequest.MLogin = Login()
        loginRequest.MLogin?.User = "1060367"//mUser //guardar usuario recibido
        loginRequest.MLogin?.Password = "Totalplay180x2$"//mPassword //guardar password recibida
        loginRequest.MLogin?.Ip = "10.213.13.60"
        
        loginRequest.NumeroEmpleado = mUser
        loginRequest.Llave = mPassword
        
        mNumeroEmpleado = mUser //*
        mPasswordEmpleado = mPassword //*
        
        RetrofitManager<LoginResponse>.init(requestUrl: ApiDefinition.WS_LOGIN, delegate:self).request(requestModel: loginRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_LOGIN){
            AlertDialog.hideOverlay()
            OnSuccessLogin(loginResponse: response as! LoginResponse)
            
           // ViewControllerUtils.presentViewController(from: mViewController, to: BienvenidaViewController.self)
        } 
        
    }
    func OnSuccessLogin (loginResponse: LoginResponse){
        //print(loginResponse.MResult?.Result)
        if loginResponse.MResult?.Result == "0" {

            UserDefaults.standard.set(mNumeroEmpleado, forKey: "mUser")
            UserDefaults.standard.set(mPasswordEmpleado, forKey: "mPassword")
            
           // mLoginDelegate.OnSuccessLogin(mUserId: loginResponse)
            savedDataR()
            
            //Cargar Home o Bienvenida-selfie:
            let allData = realm.objects(InfoEmployee.self)
            for person in allData {
                if person.userPhoto == nil {
                    let settingStoryboard: UIStoryboard = UIStoryboard(name: "BienvenidaViewController", bundle: nil)
                    let vc = settingStoryboard.instantiateViewController(withIdentifier: "BienvenidaViewController") as! BienvenidaViewController
                    //vc.numEmployeer = mNumeroEmpleado //pasar a bienvenida
                    mViewController.present(vc.self, animated: true, completion: nil)
                } else {
                    let settingStoryboard: UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
                    let vc = settingStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    mViewController.present(vc.self, animated: true, completion: nil)
                }
            }
            
        } else {
            if loginResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (loginResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
            
        }
    }
    func savedDataR(){
        let allData = realm.objects(InfoEmployee.self)
        //nEmpD = mNumeroEmpleado
        //nEmpD = "65018717"
        nEmpD = "65019503"
        //nEmpD = nEmpD?.decrypt()
        //print(allData.count)
            if allData.count == 0 {
                try! realm.write {
                    datas.NumEmployee = nEmpD! //guarda encriptado- desencriptar
                    realm.add(datas)
                    print("Added \(datas.NumEmployee) to Realm")
                }
            } else {
                try! realm.write {
                    datas.NumEmployee = nEmpD! //guarda encriptado- desencriptar
                    //realm.add(datas)
                    //print("Added \(datas.NumEmployee) to Realm")
                }
            }
        //print(allData)
    }
}
