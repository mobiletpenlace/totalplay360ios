//
//  SearchAuthorizationPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol SearchAuthorizationDelegate: NSObjectProtocol {
    func OnSuccessSearchAuthorization(searchAuthorizationResponse: SearchAuthorizationResponse)
}
class SearchAuthorizationPresenter: BaseTotalplay360Presenter {
    var mSearchAuthorizationDelegate: SearchAuthorizationDelegate!
    
    func getSearchAuthorization(mIdEmployee: String, mSearchAuthorizationDelegate: SearchAuthorizationDelegate){
        self.mSearchAuthorizationDelegate = mSearchAuthorizationDelegate
        
        let searchAuthorizationRequest = SearchAuthorizationRequest()
        
        searchAuthorizationRequest.MLogin = Login()
        searchAuthorizationRequest.MLogin?.Ip = "10.213.13.60"
        searchAuthorizationRequest.MLogin?.User = "1060367"
        searchAuthorizationRequest.MLogin?.Password = "Totalplay180x2$"
        
        searchAuthorizationRequest.IdEmployee = mIdEmployee
        
        RetrofitManager<SearchAuthorizationResponse>.init(requestUrl: ApiDefinition.WS_SEARCHAUTHORIZATION, delegate: self).request(requestModel: searchAuthorizationRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_SEARCHAUTHORIZATION){
            AlertDialog.hideOverlay()
            OnSuccessSearchAuthorization(searchAuthorizationResponse: response as! SearchAuthorizationResponse)
        }
    }
    func OnSuccessSearchAuthorization(searchAuthorizationResponse: SearchAuthorizationResponse){
        print(searchAuthorizationResponse.MResult?.Result)
        if searchAuthorizationResponse.MResult?.Result == "0" {
            mSearchAuthorizationDelegate.OnSuccessSearchAuthorization(searchAuthorizationResponse: searchAuthorizationResponse)
        } else {
            if searchAuthorizationResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (searchAuthorizationResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }
}
