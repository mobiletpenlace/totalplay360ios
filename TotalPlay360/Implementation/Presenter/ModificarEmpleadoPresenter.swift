//
//  ModificarEmpleadoPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift

protocol ModificarEmpleadoDelegate: NSObjectProtocol {
    func OnSuccessModificarEmpleado(modificarEmpleadoResponse: ModificarEmpleadoResponse)
}
class ModificarEmpleadoPresenter: BaseTotalplay360Presenter {
    var mModificarEmpleadoDelegate: ModificarEmpleadoDelegate!
    var realm = try! Realm()
    var datas = InfoEmployee()
    
    func getModificarEmpleado(mIdEmployee: String, mMail: String, mEdifice: String, mFloor: String, mPhone: String, mExtension: String, mPictureURL: String, mModificarEmpleadoDelegate: ModificarEmpleadoDelegate){
        
        self.mModificarEmpleadoDelegate = mModificarEmpleadoDelegate
        let modificarEmpleadoRequest = ModificarEmpleadoRequest()
        
        modificarEmpleadoRequest.MLogin = Login()
        modificarEmpleadoRequest.MLogin?.User = "1060367"
        modificarEmpleadoRequest.MLogin?.Password = "Totalplay180x2$"
        modificarEmpleadoRequest.MLogin?.Ip = "10.213.13.60"
        
        modificarEmpleadoRequest.MEmployeeUIE = EmployeeUIE()
        modificarEmpleadoRequest.MEmployeeUIE?.IdEmployee = mIdEmployee
        modificarEmpleadoRequest.MEmployeeUIE?.Mail = mMail
        modificarEmpleadoRequest.MEmployeeUIE?.Edifice = mEdifice
        modificarEmpleadoRequest.MEmployeeUIE?.Floor = mFloor
        modificarEmpleadoRequest.MEmployeeUIE?.Phone = mPhone
        modificarEmpleadoRequest.MEmployeeUIE?.Extension = mExtension
        modificarEmpleadoRequest.MEmployeeUIE?.PictureURL = mPictureURL
        
        
        RetrofitManager<ModificarEmpleadoResponse>.init(requestUrl: ApiDefinition.WS_UPDATEINFOEMP, delegate: self).request(requestModel: modificarEmpleadoRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_UPDATEINFOEMP){
            AlertDialog.hideOverlay()
            OnSuccessModificarEmpleado(modificarEmpleadoRespnse: response as! ModificarEmpleadoResponse)
        }
    }
    func OnSuccessModificarEmpleado(modificarEmpleadoRespnse: ModificarEmpleadoResponse){
        print(modificarEmpleadoRespnse.MResult?.Result)
        if modificarEmpleadoRespnse.MResult?.Result == "0"{
            mModificarEmpleadoDelegate.OnSuccessModificarEmpleado(modificarEmpleadoResponse: modificarEmpleadoRespnse)

        } else {
            if modificarEmpleadoRespnse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (modificarEmpleadoRespnse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }
    
}
