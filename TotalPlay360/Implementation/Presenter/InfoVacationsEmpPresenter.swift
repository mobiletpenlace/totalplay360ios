//
//  InfoVacationsEmpPresenter.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

protocol InfoVacationsEmpDelegate: NSObjectProtocol {
    func OnSuccessInfoVacations(infoVacationsEmpResponse: InfoVacationsEmpResponse)
}
class InfoVacationsEmpPresenter: BaseTotalplay360Presenter {
    var mInfoVacationsEmpDelegate: InfoVacationsEmpDelegate!
    
    func getInfoVacationsEmp(mIdEmployee: String, mInfoVacationsEmpDelegate: InfoVacationsEmpDelegate){
        self.mInfoVacationsEmpDelegate = mInfoVacationsEmpDelegate
        let infoVacationsEmpRequest = InfoVacationsEmpRequest()
        
        infoVacationsEmpRequest.MLogin = Login()
        infoVacationsEmpRequest.MLogin?.User = "1060367"
        infoVacationsEmpRequest.MLogin?.Password = "Totalplay180x2$"
        infoVacationsEmpRequest.MLogin?.Ip = "10.213.13.60"
        
        infoVacationsEmpRequest.IdEmployee = mIdEmployee
        
        RetrofitManager<InfoVacationsEmpResponse>.init(requestUrl: ApiDefinition.WS_GETINFOVACATIONS, delegate: self).request(requestModel: infoVacationsEmpRequest)
    }
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        if (requestUrl == ApiDefinition.WS_GETINFOVACATIONS){
            AlertDialog.hideOverlay()
            OnSuccessInfoVacations(infoVacationsEmpResponse: response as! InfoVacationsEmpResponse)
        }
    }
    func OnSuccessInfoVacations(infoVacationsEmpResponse: InfoVacationsEmpResponse){
        if infoVacationsEmpResponse.MResult?.Result == "0" {
            mInfoVacationsEmpDelegate.OnSuccessInfoVacations(infoVacationsEmpResponse: infoVacationsEmpResponse)
        } else {
            if infoVacationsEmpResponse.MResult == nil {
                AlertDialog.show(title: "Error", body: "Ha ocurrido un error" , view: mViewController)
            } else {
                AlertDialog.show(title: "Error", body: (infoVacationsEmpResponse.MResult?.ResultDescription!)!, view: mViewController)
            }
        }
    }
}
