//
//  CustomAlertViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 19/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift

class CustomAlertViewController: BaseViewController, RequestVacationsDelegate {

    
    @IBOutlet weak var date1: UILabel!
    
    @IBOutlet weak var date2: UILabel!
    
    @IBOutlet weak var btnConfirmar: UIButton!
    
    @IBOutlet weak var btnModificar: UIButton!
    
    //Realm:
    let realm = try! Realm()
    //Servicio Request vacations:
    var mRequestVacationsPresenter: RequestVacationsPresenter!
    
    let colorA = UIColor(red: 96/255, green: 85/255, blue: 165/255, alpha: 1.0)
    //let colorA = UIColor.red
    let colorD = UIColor.black
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func modificar_action(_ sender: UIButton) {
        //close alert
        btnModificar.titleLabel?.textColor = colorA
        btnConfirmar.titleLabel?.textColor = colorD
        self.removeFromParentViewController()
        self.view.removeFromSuperview()

    }
    
  
    @IBAction func confirmar_action(_ sender: UIButton) {
        //close alert
        btnConfirmar.titleLabel?.textColor = colorA
        btnModificar.titleLabel?.textColor = colorD
        
        //self.removeFromParentViewController()
        //self.view.removeFromSuperview()
        //Query Realm:
        var idEmployerC: String?
        var typeR = "1"
        var fecha1 = date1.text
        var fecha2 = date2.text
        
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            //encriptar para consultar servicio:
            idEmployerC = person.IdEmployee
            idEmployerC = Security.crypt(text: idEmployerC!)
            typeR = Security.crypt(text: typeR)
            fecha1 = Security.crypt(text: fecha1!)
            fecha2 = Security.crypt(text: fecha2!)
            
            mRequestVacationsPresenter.getRequestVacations(mIdEmployee: idEmployerC!, mTypeRequest: typeR, mStartDate: fecha1!, mEndDate: fecha2!, mRequestVacationsDelegate: self)
            

        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Servicio :
    override func getPresenter() -> BasePresenter? {
        mRequestVacationsPresenter = RequestVacationsPresenter(viewController: self)
        
        return mRequestVacationsPresenter
    }
    
    func OnSuccessRequestVacations(requestVacationsResponse: RequestVacationsResponse) {
        
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }


}
