//
//  Menu2ViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 26/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class Menu2ViewController: UIViewController {

    
    @IBOutlet weak var leadingConstraint2: NSLayoutConstraint!
    
    @IBOutlet weak var menuView2: UIView!
    
    var menuShowing2 = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuView2.layer.shadowOpacity = 1
        menuView2.layer.shadowRadius = 6
        
    }

    /*override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/
    
    
    @IBAction func openMenu2(_ sender: Any) {
        
            menuView2.isHidden = false
            leadingConstraint2.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        
    }
    
    
    @IBAction func regresarButon(_ sender: Any) {
        
        leadingConstraint2.constant = -140
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            
        })
        
    }
    
}
