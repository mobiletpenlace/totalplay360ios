//
//  ComunInterna1ViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 15/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ComunInterna1ViewController: UIViewController , UIScrollViewDelegate{

    @IBOutlet weak var ScrollView: UIScrollView!

    @IBOutlet weak var PageControl: UIPageControl!
  
    @IBOutlet weak var viewScrol: UIView!

    @IBOutlet weak var notifLabel: UILabel!
    
    
    let obj1 = ["imagen": "img_promo0.jpg"]
    let obj2 = ["imagen": "img_promo1.jpg"]
    let obj3 = ["imagen": "img_promo2.jpg"]
    let obj4 = ["imagen": "img_promo3.jpg"]
    
    var ItemsArray = [Dictionary<String,String>]()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        ScrollView.delegate = self
        ScrollView.isPagingEnabled = true
        ItemsArray = [obj1, obj2, obj3, obj4]
        ScrollView.contentSize = CGSize(width: self.view.bounds.size.width * CGFloat(ItemsArray.count), height: 300)
        
        ScrollView.showsHorizontalScrollIndicator = false
        loadPromociones()
        
    }
    
  
    @IBAction func boton_back(_ sender: UIButton) {
        
    navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
        
    }
    
  
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadPromociones(){
        for (index, feature) in ItemsArray.enumerated(){
            if let view = Bundle.main.loadNibNamed("ComunicacionPromocionesCollectionReusableView", owner: self, options: nil)?.first as? ComunicacionPromocionesCollectionReusableView
            {
                //view.Titulo.text = feature["titulo"]
                view.ImagenPromo.image = UIImage(named: feature["imagen"]!)
                ScrollView.addSubview(view)
                view.frame.size.width = self.view.bounds.size.width
                view.frame.size.height = self.view.bounds.size.height - CGFloat(220)
                view.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
                
                
            }
            
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
     let page = scrollView.contentOffset.x / scrollView.frame.size.width
        PageControl.currentPage = Int(page)
        
    }

  

  

}
