//
//  EdithDatas1ViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 28/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift
import Darwin

class EdithDatas1ViewController: BaseViewController, ModificarEmpleadoDelegate {

    //Servicio:
    var mModificarEmpleadoPresenter: ModificarEmpleadoPresenter!
    
    @IBOutlet weak var mImagePerfil: UIImageView!
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var mName_label: UILabel!
    //@IBOutlet weak var mSurnamePatental_label: UILabel!
    //@IBOutlet weak var mSurnameMaternal_label: UILabel!
    @IBOutlet weak var mArea_label: UILabel!
    @IBOutlet weak var mPosition_label: UILabel!
    @IBOutlet weak var mCorreo_txt: UITextField!
    @IBOutlet weak var mUbicationEdifice_txt: UITextField!
    @IBOutlet weak var mUbicationFloor_txt: UITextField!
    @IBOutlet weak var mExtension_txt: UITextField!
    @IBOutlet weak var mCellPhone_txt: UITextField!
    //Image warning:
    @IBOutlet weak var mImg_warning1: UIImageView!
    @IBOutlet weak var mImg_warning2: UIImageView!
    @IBOutlet weak var mImg_warning3: UIImageView!
    @IBOutlet weak var mImg_warning4: UIImageView!
    @IBOutlet weak var mImg_warning5: UIImageView!
    //Realm:
    let realm = try! Realm()
    //Servicio GetInfoEmp:
    var numEmployeer: String?
    var mConsultaEmpleadoPresenter: ConsultaEmpleadoPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Query Realm:
        let allData = realm.objects(InfoEmployee.self)
        
        //Show image / selfie:
        for person in allData {
            //Cargar selfie tomada:
            //mImagePerfil.isHidden = false
            mImagePerfil.layer.cornerRadius = mImagePerfil.frame.size.width / 2
            mImagePerfil.clipsToBounds = true
            // mImagePerfil.image = UIImage(data: person.userPhoto!)
            
            //Cargar datos:
            mName_label.text = person.Name
            //mSurnamePatental_label.text = person.surnamePatental
            //mSurnameMaternal_label.text = person.surnameMaternal
            mArea_label.text = person.Area
            mPosition_label.text = person.Position
            mCorreo_txt.text = person.mail
            mUbicationEdifice_txt.text = person.edifice
            mUbicationFloor_txt.text = person.floor
            mExtension_txt.text = person.extenssion
            mCellPhone_txt.text = person.phone
        }
        //print(allData)
        
    }
    
    
    @IBAction func boton_back(_ sender: UIButton) {
        //exit(0)
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    @IBAction func cancelar_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    /* @IBAction func boton_camara(_ sender: UIButton) {
     let storyboard2: UIStoryboard = UIStoryboard(name: "BienvenidaViewController", bundle: nil)
     let viewC2 = storyboard2.instantiateViewController(withIdentifier: "BienvenidaViewController")as! BienvenidaViewController
     self.present(viewC2, animated: false, completion: {})
     } */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func mCorreo_action(_ sender: Any) {
        mImg_warning1.isHidden = true
    }
    
    @IBAction func mEdifice_action(_ sender: Any) {
        mImg_warning2.isHidden = true
    }
    
    @IBAction func mFloor_action(_ sender: Any) {
        mImg_warning3.isHidden = true
    }
    
    @IBAction func mExtension_action(_ sender: Any) {
        mImg_warning4.isHidden = true
    }
    
    @IBAction func mCell_action(_ sender: Any) {
        mImg_warning5.isHidden = true
    }
    @IBAction func GuardarDatos(_ sender: Any) {
        if mCorreo_txt.text != "" && mUbicationEdifice_txt.text != "" && mUbicationFloor_txt.text != "" && mExtension_txt.text != "" && mCellPhone_txt.text != "" {
            //Guardar datos:
            let allData = realm.objects(InfoEmployee.self)
            for person in allData {
                if mCorreo_txt.text != person.mail || mUbicationEdifice_txt.text != person.edifice || mUbicationFloor_txt.text != person.floor || mExtension_txt.text != person.extenssion || mCellPhone_txt.text != person.phone {
                    //save new datas:
                    try! realm.write {
                        person.mail = mCorreo_txt.text!
                        person.edifice = mUbicationEdifice_txt.text!
                        person.floor = mUbicationFloor_txt.text!
                        person.extenssion = mExtension_txt.text!
                        person.phone = mCellPhone_txt.text!
                    }
                    //print(allData)
                    //Encriptar antes de consultar sevicio:
                    var nEmpC = person.IdEmployee
                    nEmpC = Security.crypt(text: nEmpC)
                    var mailC = mCorreo_txt.text!
                    mailC = Security.crypt(text: mailC)
                    var edificeC = mUbicationEdifice_txt.text!
                    edificeC = Security.crypt(text: edificeC)
                    var floorC = mUbicationFloor_txt.text!
                    floorC = Security.crypt(text: floorC)
                    var phoneC = mCellPhone_txt.text!
                    phoneC = Security.crypt(text: phoneC)
                    var extencionC = mExtension_txt.text!
                    extencionC = Security.crypt(text: extencionC)
                    
                    //sevicio update info emp:
                    mModificarEmpleadoPresenter.getModificarEmpleado(mIdEmployee: nEmpC, mMail: mailC, mEdifice: edificeC, mFloor: floorC, mPhone: phoneC, mExtension: extencionC, mPictureURL: "", mModificarEmpleadoDelegate: self)
                    
                    let storyboard2: UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
                    let viewC2 = storyboard2.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
                    self.present(viewC2, animated: false, completion: {})
                } else {
                    let storyboard2: UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
                    let viewC2 = storyboard2.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
                    self.present(viewC2, animated: false, completion: {})
                }
            }
            
        } else {
            if mCorreo_txt.text == "" || mUbicationEdifice_txt.text == "" || mUbicationFloor_txt.text == "" || mExtension_txt.text == "" || mCellPhone_txt.text == "" {
                
                if mCorreo_txt.text == "" {
                    mImg_warning1.isHidden = false
                    if mUbicationEdifice_txt.text == "" {
                        mImg_warning2.isHidden = false
                        if mUbicationFloor_txt.text == "" {
                            mImg_warning3.isHidden = false
                            if mExtension_txt.text == "" {
                                mImg_warning4.isHidden = false
                                if mCellPhone_txt.text == "" {
                                    mImg_warning5.isHidden = false
                                }
                            }
                        }
                    }
                } else if mUbicationEdifice_txt.text == "" {
                    mImg_warning2.isHidden = false
                    if mUbicationFloor_txt.text == "" {
                        mImg_warning3.isHidden = false
                        if mExtension_txt.text == "" {
                            mImg_warning4.isHidden = false
                            if mCellPhone_txt.text == "" {
                                mImg_warning5.isHidden = false
                            }
                        }
                    }
                } else if mUbicationFloor_txt.text == "" {
                    mImg_warning3.isHidden = false
                    if mExtension_txt.text == "" {
                        mImg_warning4.isHidden = false
                        if mCellPhone_txt.text == "" {
                            mImg_warning5.isHidden = false
                        }
                    }
                } else if mExtension_txt.text == "" {
                    mImg_warning4.isHidden = false
                    if mCellPhone_txt.text == "" {
                        mImg_warning5.isHidden = false
                    }
                }
                if mCellPhone_txt.text == "" {
                    mImg_warning5.isHidden = false
                }
            } //if principal
        }
        
    }
    //Servicio Update Info:
    override func getPresenter() -> BasePresenter? {
        mModificarEmpleadoPresenter = ModificarEmpleadoPresenter(viewController: self)
        return mModificarEmpleadoPresenter
    }
    func OnSuccessModificarEmpleado(modificarEmpleadoResponse: ModificarEmpleadoResponse) {
        
    }

}
