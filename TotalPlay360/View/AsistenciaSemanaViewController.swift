//
//  AsistenciaSemanaViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 23/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import JTAppleCalendar
import SwiftBaseLibrary
import RealmSwift

class AsistenciaSemanaViewController: BaseViewController, SearchAssistanceDelegate, UIScrollViewDelegate {

    @IBOutlet weak var calendarView2: JTAppleCalendarView!
    @IBOutlet weak var numSemana: UILabel!
    @IBOutlet weak var SelectTime: UILabel!
    @IBOutlet weak var ViewList: UIView!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var numYear: UILabel!
    
    let formatter2 = DateFormatter()
    
    @IBOutlet weak var viewOfScroll1: UIView!
    @IBOutlet weak var scroll1: UIScrollView!
    @IBOutlet weak var viewOfScroll2: UIView!
    @IBOutlet weak var scroll2: UIScrollView!
    
    //Realm:
    let realm = try! Realm()
    //Servicio search assistance
    var mSearchAssistancePresenser: SearchAssistancePresenter!
    var startWeek: String?
    var endWeek: String?
    var arrayDate : [String] = []
    var arrayStatusDay : [String] = []
    var arrayTypeDay : [String] = []
    var arrayHour : [String] = []
    
    //Colores:
    let color1 = UIColor(red: 126/255, green: 211/255, blue: 33/255, alpha: 1.0) //verde
    let color2 = UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1.0) //amarillo
    let color3 = UIColor(red: 255/255, green: 67/255, blue: 67/255, alpha: 1.0) //rojo
    let color4 = UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0) //morado
    let color5 = UIColor(red: 207/255, green: 221/255, blue: 232/255, alpha: 1.0) //gris
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //Para ocultar el view de opciones mes/seana
        ViewList.isHidden = true
        //Para mostrar inicialmente label con "Semana" como texto
        SelectTime.text = "Semana"
        //Para borde de view mes/semana
        viewBorder.layer.borderWidth = 1
        viewBorder.layer.borderColor = UIColor.darkGray.cgColor
        //Para que inicie en la fecha actual
        calendarView2.scrollToDate(Date())
        //Para que actualice correctamente el numero de semana y año al iniciar
        setupCalendarView2()
        setupCalendarViewYear()
        
        //Scroll:
        scroll1.delegate = self
        scroll1.isPagingEnabled = true
        scroll2.delegate = self
        scroll2.isPagingEnabled = true
        
        //Prueba start and End Date for this week:
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let sWeek = Date().startOfWeek
        let eWeek = Date().endOfWeek
        
        //print(dateFormatter.string(from: Date().startOfWeek!))
        //print(dateFormatter.string(from: Date().endOfWeek!))
        startWeek = dateFormatter.string(from: sWeek!)
        endWeek = dateFormatter.string(from: eWeek!)
        
        //Query Realm data:
        let allData = realm.objects(InfoEmployee.self)
        
        //for person in allData {
            //Servicio search assistance:
            mSearchAssistancePresenser.getSearchAssistance(mIdEmployee: "4176", mStartDate: startWeek!, mEndDate: endWeek!, mPeriod: "semana", mSearchAssistanceDelegate: self)
        //}
    }
    
    func setupCalendarView2(){
        //Setup calendar spacing
        calendarView2.minimumLineSpacing = 1
        calendarView2.minimumInteritemSpacing = 1
        
        //Setup labels
        calendarView2.visibleDates{ visibleDates in
            self.setupViewsOfnumWeek(from: visibleDates)
        }

    }
    
    func setupCalendarViewYear(){
        calendarView2.visibleDates{ visibleDates in
            self.setupViewOfYear(from: visibleDates)
        }
    }
    
    func setupViewsOfnumWeek(from visibleDates: DateSegmentInfo){

        let date2 = Date()
        self.numSemana.text = String(self.formatter2.calendar.component(.weekOfYear, from: date2))
    }
    
    func setupViewOfYear(from visibleDates: DateSegmentInfo){
        let date3 = Date()
        let loc = Locale(identifier: "ES")
        self.formatter2.dateFormat = "YYYY"
        self.formatter2.locale = loc
        
        self.numYear.text = self.formatter2.string(from: date3)
    }
    
//Fechas de semanas:
    //startDate of week:
    func getFirstDay(weekNumber: Int) -> String? {
        //let calendar = Calendar(identifier: .gregorian)
        var calendar = Calendar.current
        let dayComponent = NSDateComponents()
        dayComponent.weekOfYear = weekNumber
        dayComponent.weekday = 2
        //
        let numY: Int? = Int(numYear.text!)
        dayComponent.year = numY!
        var date = calendar.date(from: dayComponent as DateComponents)
        //formato de fecha
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MM-yyyy"
        return String(dateFormater.string(from: date!))
    }
    
    //EndDate of week:
    func getEndDay(weekNumber: Int) -> String?{
        let calendar = Calendar(identifier: .gregorian)
        let dayComponent = NSDateComponents()
        dayComponent.weekOfYear = weekNumber
        dayComponent.weekday = 1
        //
        let numY: Int? = Int(numYear.text!)
        dayComponent.year = numY!
        var date = calendar.date(from: dayComponent as DateComponents)
        //formato de fecha
        //formatter2.dateFormat = "yyyy MM dd"
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MM-yyyy"
        
        return String(dateFormater.string(from: date!))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func SelectWeek(_ sender: Any) {
        SelectTime.text = "Semana"

        ViewList.isHidden = true
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "AsistenciaSemanaViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "AsistenciaSemanaViewController") as! AsistenciaSemanaViewController
        self.present(settingVC, animated: false, completion: {})
    }
    
    @IBAction func SelectMonth(_ sender: Any) {
        SelectTime.text = "Mes"
        ViewList.isHidden = true
        
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "AsistenciaMesViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "AsistenciaMesViewController") as! AsistenciaMesViewController
        self.present(settingVC, animated: false, completion: {})
    }
    
    @IBAction func ShowList(_ sender: Any) {
        ViewList.isHidden = false
        
    }
    
    @IBAction func back(_ sender: UIButton) {
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "Asistencia2ViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "Asistencia2ViewController") as! Asistencia2ViewController
        self.present(settingVC, animated: false, completion: {})
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    @IBAction func previous_action(_ sender: UIButton) {
      calendarView2.scrollToSegment(.previous)
        let numWeek: Int? = Int(numSemana.text!)
        let nYear: Int? = Int(numYear.text!)
        
        var r = numWeek! - 1;
        
        if r < 1 {
            r = 52;
            let y = nYear! - 1;
            self.numYear.text = String(y)
        }
        self.numSemana.text = String(r)
        
        //cambiar de inicio y fin de semana para consultar servicio:
        print(getFirstDay(weekNumber: r))
        print(getEndDay(weekNumber: r + 1))
    }
    
    @IBAction func next_action(_ sender: UIButton) {
        calendarView2.scrollToSegment(.next)
        let numWeek: Int? = Int(numSemana.text!)
        let nYear: Int? = Int(numYear.text!)
        
        var r = numWeek! + 1;
        
        if r > 52 {
            r = 1;
            let y = nYear! + 1;
            self.numYear.text = String(y)
        }
        self.numSemana.text = String(r)
        
        //cambiar de inicio y fin de semana para consultar servicio:
        print(getFirstDay(weekNumber: r))
        print(getEndDay(weekNumber: r + 1))
    }
    
    //Servicio search assistance:
    override func getPresenter() -> BasePresenter? {
        mSearchAssistancePresenser = SearchAssistancePresenter(viewController: self)
        
        return mSearchAssistancePresenser
    }

    func OnSuccessSearchAssistance(searchAssistanceResponse: SearchAssistanceResponse) {
        
        let count1 = (searchAssistanceResponse.MDay.count) / 2 //7
        let count2 = (searchAssistanceResponse.MDay.count) //14
        //Scroll-1:
        scroll1.contentSize = CGSize(width: self.view.bounds.size.width * CGFloat(count1), height: viewOfScroll1.bounds.size.height)
        scroll1.showsHorizontalScrollIndicator = false
        
        //Scroll-2:
        scroll2.contentSize = CGSize(width: self.view.bounds.size.width * CGFloat(count1), height: viewOfScroll2.bounds.size.height)
        scroll2.showsHorizontalScrollIndicator = false
       
        //For scroll 1
        for index in 0 ..< count1 {
            if let view = Bundle.main.loadNibNamed("AsistenciaSemanaCollectionReusableView", owner: self, options: nil)?.first as? AsistenciaSemanaCollectionReusableView {
                
                //Guardar Datos:
                var sDay = (searchAssistanceResponse.MDay[index].StatusDay!)
                var tDay = (searchAssistanceResponse.MDay[index].TypeDay!)
                
                //Mostrar datos:
                if sDay == "1" {
                    if tDay == "1" {
                        view.actionCircleE.backgroundColor = color1
                    }
                }else if sDay == "2" {
                    if tDay == "1" {
                        view.actionCircleE.backgroundColor = color2
                    }
                }else if sDay == "3" {
                    if tDay == "1" {
                        view.actionCircleE.backgroundColor = color3
                    }
                }else if sDay == "4" {
                    if tDay == "1" {
                        view.actionCircleE.backgroundColor = color4
                    }
                }else if sDay == "5" {
                    if tDay == "1" {
                        view.actionCircleE.backgroundColor = color5
                    }
                }
                //Ajustar ancho de viewE
                view.widthViewE.constant = (viewOfScroll1.bounds.size.width) / 7
                view.heightViewE.constant = (viewOfScroll1.bounds.size.height)
                //Scroll 1:
                view.frame.size.width = (self.view.bounds.size.width)-(viewOfScroll1.bounds.size.width)-100
                view.frame.origin.x = CGFloat(index) * CGFloat((viewOfScroll1.bounds.size.width) / 7)
                scroll1.addSubview(view)
                viewOfScroll1.addSubview(scroll1)

            } //if
        } //for

        //For scroll 2
        //de 7 a menor que 14
        for i in count1 ..< count2 {
            if let view2 = Bundle.main.loadNibNamed("AsistenciaSemanaCollectionReusableView", owner: self, options: nil)?.first as? AsistenciaSemanaCollectionReusableView {
                
                //Guardar Datos:
                var sDay2 = (searchAssistanceResponse.MDay[i].StatusDay!)
                var tDay2 = (searchAssistanceResponse.MDay[i].TypeDay!)
                
                //Mostrar datos:
                if sDay2 == "1" {
                    if tDay2 == "2" {
                        view2.actionCircleE.backgroundColor = color1
                    }
                }else if sDay2 == "2" {
                    if tDay2 == "2" {
                        view2.actionCircleE.backgroundColor = color2
                    }
                }else if sDay2 == "3" {
                    if tDay2 == "2" {
                        view2.actionCircleE.backgroundColor = color3
                    }
                }else if sDay2 == "4" {
                    if tDay2 == "2" {
                        view2.actionCircleE.backgroundColor = color4
                    }
                }else if sDay2 == "5" {
                    if tDay2 == "2" {
                        view2.actionCircleE.backgroundColor = color5
                    }
                }
                
                //Ajustar ancho de viewE
                view2.widthViewE.constant = (viewOfScroll2.bounds.size.width) / 7
                view2.heightViewE.constant = (viewOfScroll2.bounds.size.height)
                //Scroll:
                //Scroll 1:
                view2.frame.size.width = (self.view.bounds.size.width)-(viewOfScroll2.bounds.size.width)-100
                view2.frame.origin.x = CGFloat(i-7) * CGFloat((viewOfScroll2.bounds.size.width) / 7)
                
                scroll2.addSubview(view2)
                viewOfScroll2.addSubview(scroll2)
                
            } // if 2
        } //for 2
    } //func
} //class

extension AsistenciaSemanaViewController: JTAppleCalendarViewDataSource{
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter2.dateFormat = "yyyy MM dd"
        formatter2.timeZone = Calendar.current.timeZone
        formatter2.locale = Calendar.current.locale
        
        
        //Fecha inicial del calendario
        let startDate2 = formatter2.date(from: "2018 01 01")!
        
        //Muestra el calendario hasta la fecha establecida
        let endDate2 = formatter2.date(from: "2025 12 31")!
        
        let parameters2 = ConfigurationParameters(startDate: startDate2, endDate: endDate2, numberOfRows: 1, calendar: Calendar.current, generateInDates: .off , generateOutDates: .off, firstDayOfWeek: .monday)
        
        return parameters2
    }

}
extension AsistenciaSemanaViewController: JTAppleCalendarViewDelegate {
  
        func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
            
                let cell2 = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell3", for: indexPath) as! CustomCell3
            cell2.dateLabel.text = cellState.text
            
            //Para darle color al texto cuando si pertenecen al mes mostrado
            cell2.dateLabel.textColor = UIColor.black
            
            if cellState.day == .sunday && cellState.date < .init(){
                //Para cuando pertenece
              /*  cell2.view1Color1.backgroundColor = color4 //UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0)
                cell2.view2Color2.backgroundColor = color4 //UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0)
                */
            } else if cellState.date < .init(){
                //Para cuando no pertenece
                
                //let num: Int = Int(arc4random_uniform(3))
                //var num = arrayStatusDay[indexPath]
                
            } else {
               /* cell2.view1Color1.backgroundColor = UIColor(red: 207/255, green: 221/255, blue: 232/255, alpha: 1.0)
                cell2.view2Color2.backgroundColor = UIColor(red: 207/255, green: 221/255, blue: 232/255, alpha: 1.0)
                */
            }
            
            return cell2
        }
        func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
            
            let cell2 = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell3", for: indexPath) as! CustomCell3
            
            //Setup Cell Text
            cell2.dateLabel.text = cellState.text

        }
    
}
extension Date {
    
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    static func today() -> Date {
        return Date()
    }
    
    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Next,
                   weekday,
                   considerToday: considerToday)
    }
    
    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Previous,
                   weekday,
                   considerToday: considerToday)
    }
    func get(_ direction: SearchDirection, _ weekDay: Weekday, considerToday consider: Bool = false) -> Date {
    
        let dayName = weekDay.rawValue
    
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
    
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
    
        let searchWeekdayIndex = weekdaysName.index(of: dayName)! + 1
    
        let calendar = Calendar(identifier: .gregorian)
    
        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }
    
    var nextDateComponent = DateComponents()
    nextDateComponent.weekday = searchWeekdayIndex
    
    
    let date = calendar.nextDate(after: self,
    matching: nextDateComponent,
    matchingPolicy: .nextTime,
    direction: direction.calendarSearchDirection)
    
    return date!
    }
    
    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }

    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }

    enum SearchDirection {
        case Next
        case Previous
        
        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .Next:
                return .forward
            case .Previous:
                return .backward
            }
        }
    }
    
}
