//
//  AlertDatePickerViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol ProtocolCalendar {
    func setResultOfBusinessLogic(valueSent: String)
}
class AlertDatePickerViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    var delegate: ProtocolCalendar?
    
    var numberPickerR: Int?
    //let datePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createDatePicker()
        donePressed()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func createDatePicker(){
        //formar for picker
        datePicker.datePickerMode = .date
        
        //date spanish or other lenguaje
        let loc = Locale(identifier: "es")
        self.datePicker.locale = loc
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        var components = DateComponents()
        var components2 = DateComponents()
        
        //components.year = -150
        components.day = +15
        //Agregar fecha maxima desde servicio:
        
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        //let maxxDate = Calendar.current.date(byAdding: components2, to: Date())
            datePicker.minimumDate = minDate
            //picker.maximumDate = maxxDate


        //bar button item
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: false)
        
    }
    @objc func donePressed(){
        //format
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.view.endEditing(true)
        
    }
    @IBAction func actionAcept(_ sender: UIButton) {
        //  let FullDate = dateFormatter.string(from: picker.date)
        navigationController?.popViewController(animated: true)
        dismiss(animated: false , completion: nil )
        let FullDate = dateFormatter.string(from: datePicker.date)
        delegate?.setResultOfBusinessLogic(valueSent: FullDate)
    }

}
