//
//  CarruselViewController.swift
//  TotalPlay360
//
//  Created by fer on 29/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift

class CarruselViewController: BaseViewController, ConsultaEmpleadoDelegate, ConsultaColaboradoresDelegate {
  
    //Realm:
    let realm = try! Realm()
    //Sevicio GetInfoEmploye:
    var mConsultaEmpleadoPresenter: ConsultaEmpleadoPresenter!
    var numEmpBossCrypt: String?
    
    //servicio getInfoCollaborators
    var idEmpCrypt: String?
    var mConsultaColaboradoresPresenter: ConsultaColaboradoresPresenter!
    
    @IBOutlet weak var Img1: UIImageView!
    @IBOutlet weak var Img2: UIImageView!
 
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var viewS: UIView!
    
    @IBOutlet weak var mNombreEmpLabel: UILabel!
    @IBOutlet weak var mPositionLabel: UILabel!
    @IBOutlet weak var mAreaLabel: UILabel!
    @IBOutlet weak var mEdificeFlorLabel: UILabel!
    @IBOutlet weak var mExtensionLabel: UILabel!
    @IBOutlet weak var mPhoneLabel: UILabel!
    @IBOutlet weak var mEmailLabel: UILabel!
    
    //Servicio - 2da Parte
    @IBOutlet weak var mNameJefeLabel: UILabel!
    @IBOutlet weak var mPositionJefeLabel: UILabel!
    @IBOutlet weak var mAreaJefeLabel: UILabel!
    @IBOutlet weak var mEdificeFloorJefeLabel: UILabel!
    @IBOutlet weak var mExtensionJefeLabel: UILabel!
    @IBOutlet weak var mPhoneJefeLabel: UILabel!
    @IBOutlet weak var mEmailJefeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let myColor = UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0).cgColor
        
        //Query Realm data:
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            Img1.layer.cornerRadius = Img1.frame.size.width / 2
            Img1.clipsToBounds = true
            Img1.layer.borderWidth = 2
            Img1.layer.borderColor = myColor
            //Img1.image = UIImage(data: person.userPhoto!)
            mNombreEmpLabel.text = "¡Hola " + (person.Name) + "!"
            mPositionLabel.text = person.Position
            mAreaLabel.text = person.Area
            mEdificeFlorLabel.text = (person.edifice) + " " + (person.floor)
            mExtensionLabel.text = person.extenssion
            mPhoneLabel.text = person.phone
            mEmailLabel.text = person.mail
            
            //Servicio get info empl:
             //encriptar para consultar servicio:
             numEmpBossCrypt = person.NumEmployeeBoss
             numEmpBossCrypt = Security.crypt(text: numEmpBossCrypt!)
            mConsultaEmpleadoPresenter.getConsultaEmpleado(mNumeroEmployeer: numEmpBossCrypt!, mConsultaEmpleadoDelegate: self)
            //mConsultaEmpleadoPresenter.getConsultaEmpleado(mNumeroEmployeer:person.NumEmployeeBoss, mConsultaEmpleadoDelegate: self)
            
            //Servicio get info colaboradores:
            //encriptar para consultar servicio:
            idEmpCrypt = person.IdEmployee
            idEmpCrypt = Security.crypt(text: idEmpCrypt!)
            mConsultaColaboradoresPresenter.getConsultaColaboradores(mIdEmployee: idEmpCrypt!, mConsultaColaboradoresDelegate: self)
            //mConsultaColaboradoresPresenter.getConsultaColaboradores(mIdEmployee: person.IdEmployee, mConsultaColaboradoresDelegate: self)
            
        }

        Img2.layer.cornerRadius = Img2.frame.size.width / 2
        Img2.clipsToBounds = true
        Img2.layer.borderWidth = 2
        Img2.layer.borderColor = myColor
        
    }
    
    @IBAction func boton_menu(_ sender: Any) {
        //activar menu
        
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "MenuLateralViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "MenuLateralViewController") as! MenuLateralViewController
        
        settingVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(settingVC)
        self.view.addSubview(settingVC.view)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    @IBAction func boton_ajustes(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ModificarDatosViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ModificarDatosViewController")as! ModificarDatosViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

//Servicio get info employe_jefe y get info collaborators:

    override func getPresenters() -> [BasePresenter]? {
        mConsultaEmpleadoPresenter = ConsultaEmpleadoPresenter(viewController: self)
       
        mConsultaColaboradoresPresenter = ConsultaColaboradoresPresenter(viewController: self)
        return [mConsultaEmpleadoPresenter, mConsultaColaboradoresPresenter]
    }
    func OnSuccessEmpleado(consultaEmpleadoResponse: ConsultaEmpleadoResponse) {
        //Desencriptar datos antes de mostrar:
        var nameBoss = (consultaEmpleadoResponse.MEmployee?.Name)!
            nameBoss = Security.decrypt(text: nameBoss)
        var surnamePBoss = (consultaEmpleadoResponse.MEmployee?.SurnamePatental)!
            surnamePBoss = Security.decrypt(text: surnamePBoss)
        var surnameMBoss = (consultaEmpleadoResponse.MEmployee?.SurnameMaternal)!
            surnameMBoss = Security.decrypt(text: surnameMBoss)
        var positionBoss = (consultaEmpleadoResponse.MEmployee?.Position)!
            positionBoss = Security.decrypt(text: positionBoss)
        var areaBoss = (consultaEmpleadoResponse.MEmployee?.Area)!
            areaBoss = Security.decrypt(text: areaBoss)
        var edificeBoss = (consultaEmpleadoResponse.MEmployee?.Edifice)!
            edificeBoss = Security.decrypt(text: edificeBoss)
        var floorBoss = (consultaEmpleadoResponse.MEmployee?.Floor)!
            floorBoss = Security.decrypt(text: floorBoss)
        var extesionBoss = (consultaEmpleadoResponse.MEmployee?.Extension)!
            extesionBoss = Security.decrypt(text: extesionBoss)
        var phoneBoss = (consultaEmpleadoResponse.MEmployee?.Phone)!
            phoneBoss = Security.decrypt(text: phoneBoss)
        var emailBoss = (consultaEmpleadoResponse.MEmployee?.Mail)!
            emailBoss = Security.decrypt(text: emailBoss)
        
        //Mostrar datos:
        mNameJefeLabel.text = nameBoss + " " + surnamePBoss + " " + surnameMBoss
        mPositionJefeLabel.text = positionBoss
        mAreaJefeLabel.text = areaBoss
        mEdificeFloorJefeLabel.text = edificeBoss + " Piso" + floorBoss
        mExtensionJefeLabel.text = extesionBoss
        mPhoneJefeLabel.text = phoneBoss
        mEmailJefeLabel.text = emailBoss
        
    }
//Servicio get info colaboradores:
    func OnSuccessColaboradores(consultaColaboradoresResponse: ConsultaColaboradoresResponse) {
        
        let count = consultaColaboradoresResponse.MEmployeeGC.count
        //ScrollView.contentSize = CGSize(width: 100, height: CGFloat(120) * CGFloat(count))
        ScrollView.contentSize = CGSize(width: CGFloat(160) * CGFloat(count), height: viewS.bounds.size.height)
        ScrollView.showsHorizontalScrollIndicator = false
        
        //print(count)
        for index in 0 ..< count {
            if let view = Bundle.main.loadNibNamed("OrganigramaCollectionReusableView", owner: self, options: nil)?.first as? OrganigramaCollectionReusableView {
      
                //Desencriptar datos:
                var nameCollaborator = (consultaColaboradoresResponse.MEmployeeGC[index].Name)!
                    nameCollaborator = Security.decrypt(text: nameCollaborator)
                var surnamePCollaborator = (consultaColaboradoresResponse.MEmployeeGC[index].SurnamePatental)!
                    surnamePCollaborator = Security.decrypt(text: surnamePCollaborator)
                var surnameMCollaborator = (consultaColaboradoresResponse.MEmployeeGC[index].SurnameMaternal)!
                    surnameMCollaborator = Security.decrypt(text: surnameMCollaborator)
                
                //Mostrar datos:
                view.title.text = nameCollaborator + "\n" + surnamePCollaborator + " " + surnameMCollaborator
                //view.photo.
                
                //scroll
                
                ScrollView.addSubview(view)
                //view.frame.size.height = self.view.bounds.size.height
                view.frame.size.width = self.view.bounds.size.width
                view.frame.origin.x =  CGFloat(index) * CGFloat(120)
                
                

            }
        } //for
    }
    
}


