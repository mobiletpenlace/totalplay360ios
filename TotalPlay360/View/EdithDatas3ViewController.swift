//
//  EdithDatas3ViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 28/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift
import Darwin

class EdithDatas3ViewController: BaseViewController {

    //Servicio:
    
    
    @IBOutlet weak var mImagePerfil: UIImageView!
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var mName_label: UILabel!
    //@IBOutlet weak var mSurnamePatental_label: UILabel!
    //@IBOutlet weak var mSurnameMaternal_label: UILabel!
    @IBOutlet weak var mArea_label: UILabel!
    @IBOutlet weak var mPosition_label: UILabel!
    @IBOutlet weak var mBirthdate_txt: UILabel!
    @IBOutlet weak var mRFC_txt: UILabel!
    @IBOutlet weak var mCURP_txt: UILabel!
    @IBOutlet weak var mBirthplaceCity_txt: UITextField!
    @IBOutlet weak var mBirthplaceState_txt: UITextField!
    //@IBOutlet weak var mGenre_txt: UITextField!
    @IBOutlet weak var mCivilStatus_txt: UITextField!
    //Image warning:
    @IBOutlet weak var mImg_warning1: UIImageView!
    @IBOutlet weak var mImg_warning2: UIImageView!
    @IBOutlet weak var mImg_warning3: UIImageView!
    //@IBOutlet weak var mImg_warning4: UIImageView!

    
    //Realm:
    let realm = try! Realm()
    
    //Servicio :
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Query Realm:
        let allData = realm.objects(InfoEmployee.self)
        
        //Show image / selfie:
        for person in allData {
            //Cargar selfie tomada:
            //mImagePerfil.isHidden = false
            mImagePerfil.layer.cornerRadius = mImagePerfil.frame.size.width / 2
            mImagePerfil.clipsToBounds = true
            // mImagePerfil.image = UIImage(data: person.userPhoto!)
            
            //Cargar datos:
            mName_label.text = person.Name
            //mSurnamePatental_label.text = person.surnamePatental
            //mSurnameMaternal_label.text = person.surnameMaternal
            
            //Agregar datos faltantes...
        }
        //print(allData)
        
    }
    
    
    @IBAction func boton_back(_ sender: UIButton) {
        //exit(0)
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    /* @IBAction func boton_camara(_ sender: UIButton) {
     let storyboard2: UIStoryboard = UIStoryboard(name: "BienvenidaViewController", bundle: nil)
     let viewC2 = storyboard2.instantiateViewController(withIdentifier: "BienvenidaViewController")as! BienvenidaViewController
     self.present(viewC2, animated: false, completion: {})
     } */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   /* @IBAction func mStreet_action(_ sender: Any) {
        mImg_warning1.isHidden = true
    }
    
    @IBAction func mExtNumber_action(_ sender: Any) {
        mImg_warning2.isHidden = true
    }
    
    @IBAction func mIntNumber_action(_ sender: Any) {
        mImg_warning3.isHidden = true
    }
    
    @IBAction func mColony_action(_ sender: Any) {
        mImg_warning4.isHidden = true
    }
    */

    @IBAction func GuardarDatos(_ sender: Any) {
       
        
    }
    
    @IBAction func cancelar_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    //Servicio o success..:


}
