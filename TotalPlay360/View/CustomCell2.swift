//
//  CustomCell2.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 21/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCell2: JTAppleCell {
    @IBOutlet weak var dateLabel2: UILabel!
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var viewCircle: UIView!

}
