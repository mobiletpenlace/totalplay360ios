//
//  CredencialElecViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 15/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import RealmSwift

class CredencialElecViewController: UIViewController {

    @IBOutlet weak var imagePerfil: UIImageView!
    
    @IBOutlet weak var imageQR: UIImageView!
    
    @IBOutlet weak var mName_Label: UILabel!
    
    @IBOutlet weak var mPuesto_Label: UILabel!
    
    @IBOutlet weak var mArea_Label: UILabel!
    
    let realm = try! Realm()
    
    let colorLine = UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0).cgColor
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Query realm:
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            mName_Label.text = person.Name
            mPuesto_Label.text = "Puesto: " + person.Position
            mArea_Label.text = "Área: " + person.Area
            
            //Cargar selfie tomada:
            if person.userPhoto == nil {
                imagePerfil.image = UIImage(named: "userCamera.png")
            } else {
                imagePerfil.isHidden = false
                imagePerfil.layer.borderWidth = 2
                imagePerfil.layer.borderColor = colorLine
                imagePerfil.layer.cornerRadius = imagePerfil.frame.size.width/2
                imagePerfil.clipsToBounds = true
                
                imagePerfil.image = UIImage(data: person.userPhoto!)
            }
        }
        //generar QR:
        var stringQR = (mName_Label.text)! + "\n" + (mPuesto_Label.text)! + "\n" + (mArea_Label.text)!
        let imageCode = generateQRcode(from: stringQR)
        imageQR.image = imageCode
    }

    @IBAction func boton_back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func generateQRcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.isoLatin1)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            guard let qrCodeImage = filter.outputImage else { return nil }
            let scaleX = imageQR.frame.size.width / qrCodeImage.extent.size.width
            let scaleY = imageQR.frame.size.height / qrCodeImage.extent.size.height
            let transform = CGAffineTransform (scaleX: scaleX, y: scaleY)
            if let output = filter.outputImage?.transformed(by: transform){
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
}
