//
//  BienvenidaViewController.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 23/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import Toucan
import SwiftBaseLibrary
import RealmSwift
import Darwin


class BienvenidaViewController: BaseViewController, ConsultaEmpleadoDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var ImageUser: UIImageView!
    @IBOutlet weak var viewDatas: UIView!
    
    var image: UIImage?
    
    var imagePickerController : UIImagePickerController!
    //Realm:
    let realm = try! Realm()
    let datas = InfoEmployee()

    //Sevicio GetInfoEmp:
    var numEmpCrypt: String?
    var mConsultaEmpleadoPresenter: ConsultaEmpleadoPresenter!
    @IBOutlet weak var mNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDatas.layer.shadowColor = UIColor.gray.cgColor
        viewDatas.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewDatas.layer.shadowOpacity = 0.8

        //Query Realm data:
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            //Servicio GetInfoEmp:
             //encriptar para consultar servicio:
             numEmpCrypt = person.NumEmployee
             numEmpCrypt = Security.crypt(text: numEmpCrypt!)
            //mConsultaEmpleadoPresenter.getConsultaEmpleado(mNumeroEmployeer:person.NumEmployee, mConsultaEmpleadoDelegate: self)
            mConsultaEmpleadoPresenter.getConsultaEmpleado(mNumeroEmployeer: numEmpCrypt!, mConsultaEmpleadoDelegate: self)
            
            //Cargar selfie tomada:
            if person.userPhoto == nil {
                ImageUser.image = UIImage(named: "userCamera.png")
            } else {
                ImageUser.image = UIImage(data: person.userPhoto!)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func boton_back(_ sender: UIButton) {
        //UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
        exit(0)
    }
    
    @IBAction func TakePhoto(_ sender: Any) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        imagePickerController.cameraDevice = .front
        imagePickerController.showsCameraControls = true
        imagePickerController.allowsEditing = true
        
        present(imagePickerController, animated: true, completion: nil)
        
        /*/show alert
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertCameraViewController") as! CustomAlertCameraViewController
        
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(vc)
        self.view.addSubview(vc.view) */
    }
    
    @IBAction func boton_photo(_ sender: UIButton) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        imagePickerController.cameraDevice = .front
        imagePickerController.showsCameraControls = true
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
        /*/show alert
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertCameraViewController") as! CustomAlertCameraViewController
        
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(vc)
        self.view.addSubview(vc.view) */
        
    }
    
    @IBAction func SavePhoto(_ sender: Any) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ModificarDatosViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ModificarDatosViewController")as! ModificarDatosViewController
        self.present(viewC2, animated: false, completion: {})
        
      /*  if ImageUser.image != UIImage(named: "userCamera.png") {
    
            let storyboard2: UIStoryboard = UIStoryboard(name: "ModificarDatosViewController", bundle: nil)
            let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ModificarDatosViewController")as! ModificarDatosViewController
            self.present(viewC2, animated: false, completion: {})
        } else {
            AlertDialog.show(title: "", body: "Tómate una selfie para continuar", view: self)
        }
       
       */
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            saveSelectedImage(selectedImage)
            ImageUser.image = selectedImage

            ImageUser.imageBackRound(imageRound: (info[UIImagePickerControllerOriginalImage] as? UIImage)!, x: Double(ImageUser.frame.height + 50), y: Double(ImageUser.frame.height + 50), widthRadius: 2)
            dismiss(animated: true, completion: nil)
        }
    }
    
  
    // Set Selected Image
    func saveSelectedImage(_ image : UIImage) {
        
        ImageUser.image = image
        let imageData = UIImageJPEGRepresentation(image, 100)!
        
        let allData = realm.objects(InfoEmployee.self)
        if ImageUser.image == UIImage(named: "userCamera.png") {
            for person in allData {
                try! realm.write {
                    person.userPhoto = imageData
                    realm.add(person)
                }
                
            }
            //print(allData)
        } else {
            for person in allData {
                try! realm.write {
                    person.userPhoto = imageData
                    //realm.add(person)
                }
            }
        }
    }
    
    //Servicio getInfoEmp:
    override func getPresenter() -> BasePresenter? {
        mConsultaEmpleadoPresenter = ConsultaEmpleadoPresenter(viewController: self)
        return mConsultaEmpleadoPresenter
    }
    func OnSuccessEmpleado(consultaEmpleadoResponse: ConsultaEmpleadoResponse) {
        //desencriptar datos del servicio:
        var name = (consultaEmpleadoResponse.MEmployee?.Name)!
            name = Security.decrypt(text: name)
        var idEmp = (consultaEmpleadoResponse.MEmployee?.IdEmployee)!
            idEmp = Security.decrypt(text: idEmp)
        var idBoss = (consultaEmpleadoResponse.MEmployee?.IdBoss)!
            idBoss = Security.decrypt(text: idBoss)
        var numEmpBoss = (consultaEmpleadoResponse.MEmployee?.NumEmployeeBoss)!
            numEmpBoss = Security.decrypt(text: numEmpBoss)
        var surnameP = (consultaEmpleadoResponse.MEmployee?.SurnamePatental)!
            surnameP = Security.decrypt(text: surnameP)
        var surnameM = (consultaEmpleadoResponse.MEmployee?.SurnameMaternal)!
            surnameM = Security.decrypt(text: surnameM)
        var area = (consultaEmpleadoResponse.MEmployee?.Area)!
            area = Security.decrypt(text: area)
        var position = (consultaEmpleadoResponse.MEmployee?.Position)!
            position = Security.decrypt(text: position)
        var mail = (consultaEmpleadoResponse.MEmployee?.Mail)!
            mail = Security.decrypt(text: mail)
        var edifice = (consultaEmpleadoResponse.MEmployee?.Edifice)!
            edifice = Security.decrypt(text: edifice)
        var floor = (consultaEmpleadoResponse.MEmployee?.Floor)!
            floor = Security.decrypt(text: floor)
        var extenssion = (consultaEmpleadoResponse.MEmployee?.Extension)!
            extenssion = Security.decrypt(text: extenssion)
        var phone = (consultaEmpleadoResponse.MEmployee?.Phone)!
            phone = Security.decrypt(text: phone)
        var latitude = (consultaEmpleadoResponse.MEmployee?.Latitude)!
            latitude = Security.decrypt(text: latitude)
        var longitude = (consultaEmpleadoResponse.MEmployee?.Longitude)!
            longitude = Security.decrypt(text: longitude)
        
        //Cargar nombre desencriptado:
        mNameLabel.text = name

        
        //Guardar datos de get info employeer desencriptados en realm:
        
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            if datas.NumEmployee != nil {
                try! realm.write {                 
                    person.IdEmployee = idEmp
                    person.IdBoss = idBoss
                    person.NumEmployeeBoss = numEmpBoss
                    person.Name = name
                    person.surnamePatental = surnameP
                    person.surnameMaternal = surnameM
                    person.Area = area
                    person.Position = position
                    person.mail = mail
                    person.edifice = edifice
                    person.floor = floor
                    person.extenssion = extenssion
                    person.phone = phone
                    person.latitude = latitude
                    person.longitude = longitude
                    
                }
            }
            
        }
        print(allData)
    }
    
}

extension UIImageView
{
    func imageBackRound(imageRound : UIImage , x : Double , y : Double , widthRadius : Double)
    {
        var imageRoundEdited = Toucan(image: imageRound).resize(CGSize(width: x, height: y), fitMode: Toucan.Resize.FitMode.crop).image
        imageRoundEdited = Toucan(image: imageRoundEdited!).maskWithEllipse(borderWidth: CGFloat(widthRadius), borderColor: UIColor.white).image
        image = imageRoundEdited
    }
}
