//
//  Asistencia2ViewController.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 01/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SwiftBaseLibrary
import RealmSwift

final class Annotations: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        
        super.init()
    }
}
class Asistencia2ViewController: BaseViewController, CLLocationManagerDelegate, GetAddressCDelegate, RegisterAssistanceDelegate {

    

    
    //Realm:
    let realm = try! Realm()
    //Servicio getAddress:
    var mGetAddressCPresenter: GetAddressCPresenter!
    var arrayLatitude: [String] = []
    var arrayLongitude: [String] = []
    
    //Sevicio RegisterAssistance:
    var mRegisterAssistancePresenter: RegisterAssistancePresenter!
    var registro: Int?
    var arrayDistance: [Double] = []
    
    @IBOutlet weak var rgsEntrada: UIButton!
    
    @IBOutlet weak var rgsSalida: UIButton!
    
    @IBOutlet weak var verResumen: UIButton!

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var fechaActual: UILabel!
    
    let locationManager = CLLocationManager()
    
    //Declaración de colores para botones registrar entrada y registrar salida
    let btnColor1 = UIColor(red: 107/255, green: 36/255, blue: 255/255, alpha: 1.0)
    
    let btnColor2 = UIColor.white
    
    let btnColor3 = UIColor(red: 125/255, green: 125/255, blue: 125/255, alpha: 1.0)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       //Background and Border color botones al iniciar la pantalla
        rgsSalida.layer.borderWidth = 1
        rgsSalida.layer.borderColor = btnColor3.cgColor
        
        verResumen.layer.borderWidth = 1
        verResumen.layer.borderColor = btnColor3.cgColor
        
        //Para mapa
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        //Para obtener fecha actual en Label
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd - MMMM - yyyy"
        let loc = Locale(identifier: "es")
        formatter.locale = loc
        let result = formatter.string(from: date)
        let label = String(result)
        fechaActual.text = label
        
        //Servicio get Address:
        mGetAddressCPresenter.getAddressC(mGetAddressCDelegate: self)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
        let location = locations[0]
        let center = location.coordinate
        
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: center, span: span)
        
        //locations:
        let countA = arrayLatitude.count
        
        for index in 0 ..< countA {
            let miCoordinate = CLLocationCoordinate2DMake(Double(arrayLatitude[index])!, Double(arrayLongitude[index])!)
            let annotation = Annotations(coordinate: miCoordinate)
            
            mapView.addAnnotation(annotation)
        }
        
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func ViewResumen(_ sender: Any) {
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "AsistenciaSemanaViewController", bundle: nil)
        let settingVC =  settingStoryboard.instantiateViewController(withIdentifier: "AsistenciaSemanaViewController") as! AsistenciaSemanaViewController
        self.present(settingVC, animated: false, completion: {
            
        })
    }
    
    @IBAction func btnEntrada(_ sender: UIButton) {
        rgsEntrada.layer.borderWidth = 1
        rgsEntrada.layer.borderColor = btnColor1.cgColor
        rgsEntrada.backgroundColor = btnColor1
        rgsEntrada.titleLabel?.textColor = btnColor2
        //-----------------------------
        rgsSalida.layer.borderWidth = 1
        rgsSalida.layer.borderColor = btnColor3.cgColor
        rgsSalida.backgroundColor = btnColor2
        rgsSalida.titleLabel?.textColor = btnColor3
        
        //Custom Alert
        
            //show alert
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert2ViewController") as! CustomAlert2ViewController
            
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.addChildViewController(vc)
            self.view.addSubview(vc.view)
        
        //Servicio Register Assistance:
        let countB = arrayLatitude.count
        var myCoordinate = CLLocation(latitude: mapView.userLocation.coordinate.latitude, longitude: mapView.userLocation.coordinate.longitude)
        //var myCoordinate = CLLocation(latitude: 19.304794, longitude: -99.204449)
        
        for index in 0 ..< countB {
            //let otherCoordinate = CLLocationCoordinate2DMake(Double(arrayLatitude[index])!, Double(arrayLongitude[index])!)
            var otherCoordinate = CLLocation(latitude: Double(arrayLatitude[index])!, longitude: Double(arrayLongitude[index])!)
            
            var distance = myCoordinate.distance(from: otherCoordinate) // /1000
            
            arrayDistance.append(distance)
        }
        //Encontrar la distancia menor:
        var mayor: Double = 0
        var menor: Double = 0
        for i in 0 ..< arrayDistance.count{
            if mayor < arrayDistance[i] {
                mayor = arrayDistance[i]
            }
        }
        menor = mayor
        for i in 0 ..< arrayDistance.count{
            if menor > arrayDistance[i] {
                menor = arrayDistance[i]
            }
        }
        if menor < 200 {
            //se consulta servicio:
            //print("consulta exitosa")
            let allData = realm.objects(InfoEmployee.self)
            for person in allData {
                print(person.IdEmployee)
                mRegisterAssistancePresenter.getRegisterAssistance(mIdEmployee: person.IdEmployee, mTypeAssistance: "1", mRegisterAssistanceDelegate: self)
            }
            
        } else {
            //Enviar alert
            print("consulta erronea")
        }
    }
    
    @IBAction func btnSalida(_ sender: UIButton) {
        rgsSalida.layer.borderWidth = 1
        rgsSalida.layer.borderColor = btnColor1.cgColor
        rgsSalida.backgroundColor = btnColor1
        rgsSalida.titleLabel?.textColor = UIColor.white
        //-----------------------------
        rgsEntrada.layer.borderWidth = 1
        rgsEntrada.layer.borderColor = btnColor3.cgColor
        rgsEntrada.backgroundColor = btnColor2
        rgsEntrada.titleLabel?.textColor = btnColor3
        
        //Custom Alert
        
        //show alert
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlert2ViewController") as! CustomAlert2ViewController
        
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.addChildViewController(vc)
        self.view.addSubview(vc.view)
    
        //Servicio Register Assistance:
        let countB = arrayLatitude.count
        var myCoordinate = CLLocation(latitude: mapView.userLocation.coordinate.latitude, longitude: mapView.userLocation.coordinate.longitude)
        //var myCoordinate = CLLocation(latitude: 19.304794, longitude: -99.204449)
        
        for index in 0 ..< countB {
            //let otherCoordinate = CLLocationCoordinate2DMake(Double(arrayLatitude[index])!, Double(arrayLongitude[index])!)
            var otherCoordinate = CLLocation(latitude: Double(arrayLatitude[index])!, longitude: Double(arrayLongitude[index])!)
            
            var distance = myCoordinate.distance(from: otherCoordinate) // /1000
            
            arrayDistance.append(distance)
        }
        //Encontrar la distancia menor:
        var mayor: Double = 0
        var menor: Double = 0
        for i in 0 ..< arrayDistance.count{
            if mayor < arrayDistance[i] {
                mayor = arrayDistance[i]
            }
        }
        menor = mayor
        for i in 0 ..< arrayDistance.count{
            if menor > arrayDistance[i] {
                menor = arrayDistance[i]
            }
        }
        if menor < 200 {
            //se consulta servicio:
            let allData = realm.objects(InfoEmployee.self)
            for person in allData {
                
                mRegisterAssistancePresenter.getRegisterAssistance(mIdEmployee: person.IdEmployee, mTypeAssistance: "2", mRegisterAssistanceDelegate: self)
            }
            //print("consulta exitosa")
        } else {
            //Enviar alert
            print("consulta erronea")
        }
    }
    
   
    @IBAction func boton_back(_ sender: Any) {
        //navigationController?.popViewController(animated: true)
        //dismiss(animated: false, completion: nil)
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.present(settingVC, animated: false, completion: {})
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    //Servicios:
    override func getPresenters() -> [BasePresenter]? {
        mGetAddressCPresenter = GetAddressCPresenter(viewController: self)
        mRegisterAssistancePresenter = RegisterAssistancePresenter(viewController: self)
        
        return[mGetAddressCPresenter, mRegisterAssistancePresenter]
    }
    
    func OnSuccessGetAddressC(getAddressCResponse: GetAddressCResponse){
        let count = getAddressCResponse.MAddressCatalog.count
        for index in 0 ..< count {
            arrayLatitude.append(getAddressCResponse.MAddressCatalog[index].latitude!)
            arrayLongitude.append(getAddressCResponse.MAddressCatalog[index].longitude!)
        }
    }
    
    func OnSuccessRegisterAssistance(registerAssistanceResponse: RegisterAssistanceResponse) {
        
    }
}
