//
//  CustomAlert2ViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 08/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CustomAlert2ViewController: UIViewController {

    @IBOutlet weak var viewAlert: UILabel!
    //var date = NSDate
    
    @IBOutlet weak var btnAceptar: UIButton!
    
    let color = UIColor(red: 96/255, green: 85/255, blue: 165/255, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    //Obtener fecha de sistema:
        let date = Date()
        let formatter = DateFormatter()
        //linea para establecer formato de fecha:
        formatter.dateFormat = "dd-MMMM-yyyy"
        let loc = Locale(identifier: "es")
        formatter.locale = loc
        //obtener resultado
        let result = formatter.string(from: date)
        //Obtener hora de sistema:
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        
        //set the label
        let label = "Tu ingreso del día de hoy\n" + String(result) + " ha sido\nregistrado a las " + String(hour) + ":" + String(minutes) + " horas"
        viewAlert.text = label
    }

    @IBAction func aceptar_action(_ sender: UIButton) {
        //aceptar alert
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
        //Colo letra al pulsar
        btnAceptar.titleLabel?.textColor = color
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  

}
