//
//  LoginViewController.swift
//  TotalPlay360
//
//  Created by fer on 30/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary //s
import LocalAuthentication
import RealmSwift

class LoginViewController: BaseViewController, LoginDelegate, UIAlertViewDelegate{
    
    var mLoginPresenter: LoginPresenter! //s
    var nEmp : String?
    var nPass: String?
    
    //Realm
    let datas = InfoEmployee()
    let realm = try! Realm()
    var opcion = 1
    @IBOutlet weak var NumeroEmpTextField: UITextField! //s
    @IBOutlet weak var mPasswordTextField: UITextField! //s
    
    @IBOutlet weak var NumeroEmpView: UIView!
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var NumeroEmpLabel: UILabel!
    @IBOutlet weak var UsuarioImage: UIImageView!
    @IBOutlet weak var buttonNoSoyYo: UIButton!
    
    let colorLine = UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0).cgColor
    
    override func viewDidLoad() {
        super.viewDidLoad()

            UsuarioImage.isHidden = true
            UsuarioImage.layer.cornerRadius = UsuarioImage.frame.size.width / 2
            UsuarioImage.clipsToBounds = true
            buttonNoSoyYo.isHidden = true
        //let allData = realm.objects(Human.self)
        if UserDefaults.standard.object(forKey: "mUser") != nil {
            IDTouch()
            let allData = realm.objects(InfoEmployee.self)
            for person in allData {
                //Cargar selfie tomada:
                UsuarioImage.isHidden = false
                UsuarioImage.layer.borderWidth = 2
                UsuarioImage.layer.borderColor = colorLine
                UsuarioImage.layer.cornerRadius = UsuarioImage.frame.size.width/2
                UsuarioImage.clipsToBounds = true
                //UsuarioImage.image = UIImage(data: person.userPhoto!)
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func IDTouch(){
        let context: LAContext = LAContext()
        
        
        if(context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)){
            var localizedReason = "Please verify yourself"
            
            if #available(iOS 11.0, *) {
                switch context.biometryType {
                case .faceID: localizedReason = "Unlock using Face ID"
                case .touchID: localizedReason = "Unlock using Touch ID"
                case .none: print("No Biometric support")
                }
            } else {
                
            }
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: localizedReason, reply: { (wasSuccesful, error) -> Void in
                if (wasSuccesful){
                    DispatchQueue.main.sync {
                        if let y = UserDefaults.standard.object(forKey: "mPassword") as? String{
                            self.mPasswordTextField.text = y
                            self.login()
                        }
                    }
                }
            })
        }
    }

    @IBAction func actionNumeroEmpTxt(_ sender: Any) {
        NumeroEmpView.layer.backgroundColor = colorLine
        PasswordView.layer.backgroundColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func actionPasswordTxt(_ sender: Any) {
        PasswordView.layer.backgroundColor = colorLine
        NumeroEmpView.layer.backgroundColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func test(_ sender: Any) {
        login()
        //savedDataR()
    }
    
    @IBAction func actionNoSoyYo(_ sender: UIButton) {
        UsuarioImage.isHidden = true
        NumeroEmpTextField.text = ""
        NumeroEmpTextField.isHidden = false
        mPasswordTextField.text = ""
        NumeroEmpView.isHidden = false
        NumeroEmpLabel.isHidden = false
        buttonNoSoyYo.isHidden = true
        
        try! realm.write {
            realm.deleteAll()
        }
        
    }
    func login(){
        if NumeroEmpTextField.text == "" || mPasswordTextField.text == "" {
            
            AlertDialog.show(title: "Error", body: "Favor de ingresar todos los datos", view: self)
            
        } else {
            nEmp = NumeroEmpTextField.text
            nPass = mPasswordTextField.text
            
            nEmp = nEmp?.crypt()
            nPass = nPass?.crypt()
            print(nEmp)
            print(nPass)
            
            //Cargar Home o Bienvenida-selfie:
            mLoginPresenter.getLogin(mUser: nEmp! , mPassword: nPass!, mLoginDelegate: self)
        }
        
    }
  
    override func viewDidAppear(_ animated: Bool) {
        if var x = UserDefaults.standard.object(forKey: "mUser") as? String {
            let allData = realm.objects(InfoEmployee.self)
            if datas.NumEmployee != nil {
                //x.decrypt() desencriptar x antes de pasar a textfield
                //x = "65018717"
                x = "65019503"
                NumeroEmpTextField.text = x
                NumeroEmpTextField.isHidden = true
                NumeroEmpView.isHidden = true
                NumeroEmpLabel.isHidden = true
                buttonNoSoyYo.isHidden = false
            }
            
        }
    }

//servicio Login:
    
    override func getPresenter() -> BasePresenter? {
        mLoginPresenter = LoginPresenter(viewController: self)
        return mLoginPresenter
    }
    
    func OnSuccessLogin(mUserId: LoginResponse) {
        AlertDialog.show(title: "Response", body: mUserId.userid!, view: self)
        //savedDataR()
    }

    
}
