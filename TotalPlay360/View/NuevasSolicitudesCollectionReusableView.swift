//
//  NuevasSolicitudesCollectionReusableView.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 26/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class NuevasSolicitudesCollectionReusableView: UICollectionReusableView {
 
    
    @IBOutlet weak var fechaInicio: UILabel!
    @IBOutlet weak var fechaFin: UILabel!
    @IBOutlet weak var autorizador: UILabel!
    @IBOutlet weak var fechaAutorizador: UILabel!
    @IBOutlet weak var estatus: UILabel!
    
    @IBOutlet weak var imageHolidays: UIImageView!

    @IBOutlet weak var viewFechas: UIView!
    @IBOutlet weak var constraintViewHolidays: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageHolidays.layer.cornerRadius = 10
        
        imageHolidays.clipsToBounds = true
        
        //viewFechas
        viewFechas.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    
    }
}
