//
//  MiCapacitacionViewController.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 20/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class MiCapacitacionViewController: UIViewController {
    


    override func viewDidLoad() {
        super.viewDidLoad()

      
    }
    
   
    @IBAction func boton_menu(_ sender: Any) {
        //activar menu
        
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "MenuLateralViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "MenuLateralViewController") as! MenuLateralViewController
        //self.present(settingVC, animated: false, completion: {})
        
        settingVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(settingVC)
        self.view.addSubview(settingVC.view)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
