//
//  ViewController.swift
//  TotalPlay360
//
//  Created by fer on 15/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import JTAppleCalendar

class AsistenciaMesViewController: UIViewController {
    
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthYear: UILabel!
    @IBOutlet weak var SelectTime: UILabel!
    @IBOutlet weak var ViewList: UIView!
    
    @IBOutlet weak var viewBorder: UIView!
    
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Para ocultar el view de opciones mes/seana
        ViewList.isHidden = true
        //Para mostrar inicialmente label con "Mes" como texto
        SelectTime.text = "Mes"
        //Para borde de view mes/semana
        viewBorder.layer.borderWidth = 1
        viewBorder.layer.borderColor = UIColor.darkGray.cgColor
        //Para que inicie en la fecha actual
        calendarView.scrollToDate(Date())
        //Para que actualice correctamente el nombre del mes al iniciar
        setupCalendarView()
    }
    func setupCalendarView(){
        //Setup calendar spacing
        calendarView.minimumLineSpacing = 1
        calendarView.minimumInteritemSpacing = 1
        
        //Setup labels
        calendarView.visibleDates{ visibleDates in
            self.setupViewsOfCalendar(from: visibleDates)
        }
        
        
       
    }
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo){
        
        let date = visibleDates.monthDates.first?.date
        let loc = Locale(identifier: "ES")
        self.formatter.dateFormat = "MMMM yyyy"
        self.formatter.locale = loc
        
        self.monthYear.text = self.formatter.string(from: date!)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    @IBAction func SelectWeek(_ sender: Any) {
        SelectTime.text = "Semana"
        ViewList.isHidden = true
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "AsistenciaSemanaViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "AsistenciaSemanaViewController") as! AsistenciaSemanaViewController
        self.present(settingVC, animated: false, completion: {})
    }
    
    @IBAction func SelectMonth(_ sender: Any) {
        SelectTime.text = "Mes"
        ViewList.isHidden = true
        
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "AsistenciaMesViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "AsistenciaMesViewController") as! AsistenciaMesViewController
        self.present(settingVC, animated: false, completion: {})
        
    }
    
    @IBAction func ShowList(_ sender: Any) {
        ViewList.isHidden = false
        
    }
    
    @IBAction func back(_ sender: UIButton) {
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "Asistencia2ViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "Asistencia2ViewController") as! Asistencia2ViewController
        self.present(settingVC, animated: false, completion: {})
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    @IBAction func previous_action(_ sender: UIButton) {
        calendarView.scrollToSegment(.previous)
        
    }
    
    @IBAction func next_action(_ sender: UIButton) {
        calendarView.scrollToSegment(.next)
    }
}

extension AsistenciaMesViewController: JTAppleCalendarViewDataSource{
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        
        //Fecha inicial del calendario
        let startDate = formatter.date(from: "2018 01 01")!

        //Muestra el calendario hasta la fecha establecida
        let endDate = formatter.date(from: "2028 12 31")!
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 6, calendar: Calendar.current, generateInDates: .forAllMonths , generateOutDates: .tillEndOfGrid , firstDayOfWeek: .monday)
        
        return parameters
    }

    
}
extension AsistenciaMesViewController: JTAppleCalendarViewDelegate {
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell2", for: indexPath) as! CustomCell2
        //Setup Cell Text
        cell.dateLabel2.text = cellState.text
        
        //Setup  color
        if cellState.dateBelongsTo == .thisMonth {
            
            //Para llamar funcion que asigna nombre del mes al label superior
            setupCalendarView()
            
            //Para darle color al texto cuando si pertenecen al mes mostrado
            cell.dateLabel2.textColor = UIColor.black

            //Color para views para cuando es parte del mes que se muestra
            cell.viewLabel.backgroundColor = UIColor(red: 243/255, green: 244/255, blue: 245/255, alpha: 1.0)
            cell.viewCircle.backgroundColor = UIColor(red: 243/255, green: 244/255, blue: 245/255, alpha: 1.0)
            
            
            if cellState.day == .sunday && cellState.date < .init(){
                //Para cuando pertenece
                cell.viewColor.backgroundColor = UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0)
            } else if cellState.date < .init(){
                
                let num: Int = Int(arc4random_uniform(3))
                
                
                    if num == 0 {
                        cell.viewColor.backgroundColor = UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1.0)
                        
                    } else if num == 1 {
                        cell.viewColor.backgroundColor = UIColor(red: 126/255, green: 211/255, blue: 33/255, alpha: 1.0)
                    } else if num == 2{
                         cell.viewColor.backgroundColor = UIColor(red: 255/255, green: 67/255, blue: 67/255, alpha: 1.0)
                    }
            } else {
                cell.viewColor.backgroundColor = UIColor(red: 207/255, green: 221/255, blue: 232/255, alpha: 1.0)
            }

        } else {
            //Para que no se vea el numero y circuloView cuando no pertenecen al mes mostrado
            cell.dateLabel2.textColor = UIColor.white
            cell.viewColor.backgroundColor = UIColor.white
            //Para ocultar los views cuando los dias no pertenecen al mes actual.
            cell.viewLabel.backgroundColor = UIColor.white
            cell.viewCircle.backgroundColor = UIColor.white
            
        }

        return cell
    }
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell2", for: indexPath) as! CustomCell2
        
        //Setup Cell Text
        cell.dateLabel2.text = cellState.text
    

    }
    func calendar(_calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo){
        setupViewsOfCalendar(from: visibleDates)

    }
    
}



