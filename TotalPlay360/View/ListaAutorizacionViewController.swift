//
//  ListaAutorizacionViewController.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 06/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift

class ListaAutorizacionViewController: BaseViewController, SearchAuthorizationDelegate, SendAuthorizationDelegate {

    
    @IBOutlet weak var ScrollView: UIScrollView!
    //Realm:
    let realm = try! Realm()
    
    //Servicio search authorization:
    var mAuthorizationPresenter: SearchAuthorizationPresenter!
    var idRequestA: [String] = []
    var idEmployerA: [String] = []
    
    //Servicio send authorizathion:
    var mSendAuthorizationPresenter: SendAuthorization!
    //var idEmp: String?
    //var idReq: String?
    var statusReq: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // ajustesAutorizaciones()
       // loadAutorizaciones()
        
        //Query realm:
        var idEmpC: String?
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            //Servicio search autorization:
            //encriptar para consultar servicio:
            idEmpC = person.IdEmployee
            idEmpC = Security.crypt(text: idEmpC!)
            mAuthorizationPresenter.getSearchAuthorization(mIdEmployee: idEmpC!, mSearchAuthorizationDelegate: self)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func getPresenters() -> [BasePresenter]? {
        mAuthorizationPresenter = SearchAuthorizationPresenter(viewController: self)
        mSendAuthorizationPresenter = SendAuthorization(viewController: self)
        return [mAuthorizationPresenter, mSendAuthorizationPresenter]
    }
//servicio sear authorizations:
    func OnSuccessSearchAuthorization(searchAuthorizationResponse: SearchAuthorizationResponse) {
        
        let count = searchAuthorizationResponse.MVacationsEmployeeSA.count

        //ScrollView.showsHorizontalScrollIndicator = false
        ScrollView.contentSize = CGSize(width: 100, height: CGFloat(100) * CGFloat(count))
        
        for index in 0 ..< count {
            if let view = Bundle.main.loadNibNamed("AutorizacionCollectionReusableView", owner: self, options: nil)?.first as? AutorizacionCollectionReusableView {
                
                //desencriptar datos del servicio:
                var idRequest = (searchAuthorizationResponse.MVacationsEmployeeSA[index].IdRequested)!
                    idRequest = Security.decrypt(text: idRequest)
                var typeR = (searchAuthorizationResponse.MVacationsEmployeeSA[index].TypeRequest)!
                    typeR = Security.decrypt(text: typeR)
                var dateR = (searchAuthorizationResponse.MVacationsEmployeeSA[index].DateRequest)!
                    dateR = Security.decrypt(text: dateR)
                var idEmp = (searchAuthorizationResponse.MVacationsEmployeeSA[index].idEmployee)!
                    idEmp = Security.decrypt(text: idEmp)
                var nameEmp = (searchAuthorizationResponse.MVacationsEmployeeSA[index].NameEmployee)!
                    nameEmp = Security.decrypt(text: nameEmp)
                var startD = (searchAuthorizationResponse.MVacationsEmployeeSA[index].StartDate)!
                    startD = Security.decrypt(text: startD)
                var endD = (searchAuthorizationResponse.MVacationsEmployeeSA[index].EndDate)!
                    endD = Security.decrypt(text: endD)
                
                //llenar arrays para botones:
                idRequestA.append(searchAuthorizationResponse.MVacationsEmployeeSA[index].IdRequested!)
                idEmployerA.append(searchAuthorizationResponse.MVacationsEmployeeSA[index].idEmployee!)
                
                
                //calcular días:
                //format date:
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .short
                dateFormatter.timeStyle = .none
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let sDate = dateFormatter.date(from: startD)
                let eDate = dateFormatter.date(from: endD)
                let calendar = Calendar.current
                let day1 = calendar.component(.day, from: sDate!)
                let day2 = calendar.component(.day, from: eDate!)
                let daysBetween: Int?
                
                if sDate != eDate {
                    daysBetween = day2 - day1
                } else {
                    daysBetween = (day2 - day1) + 1
                }
                
                let calendar2 = Calendar.current
                let d1 = calendar2.component(.weekday, from: sDate!)
                let d2 = calendar2.component(.weekday, from: eDate!)
                let dB = (d2 - d1)
                
                //Mostar datos desencriptados:
                view.Solicitante.text = nameEmp
                view.FechaInicio.text = startD
                view.FechaFin.text = endD
                view.NumeroDias.text = String(describing: (daysBetween! + dB))
                //Buttons:
                view.button_authorize.tag = index
                view.button_deny.tag = index
                view.button_authorize.addTarget(self, action: #selector(ListaAutorizacionViewController.actionAutorizar), for: UIControlEvents.touchUpInside)
                view.button_deny.addTarget(self, action: #selector(ListaAutorizacionViewController.actionRechazar), for: UIControlEvents.touchUpInside)
                
                //scroll view:
                ScrollView.addSubview(view)
                view.frame.size.height = CGFloat(100)
                view.frame.size.width = self.view.bounds.size.width
                view.frame.origin.y = CGFloat(index) * CGFloat(100)
                
            }
        }
    }
//servicio send authorizations:
    func OnSuccessSendAuthorization(sendAuthorizationResponse: SendAuthorizationResponse) {
        
        ScrollView.reloadInputViews()
    }
    @objc func actionAutorizar(sender: UIButton){
        let idR = idRequestA[sender.tag]
        let idEmpl = idEmployerA[sender.tag]
        var statusR = "2"
        statusR = Security.crypt(text: statusR)
        //servicio send authorization:
        mSendAuthorizationPresenter.getSendAuthorization(mIdRequested: idR, mIdEmployee: idEmpl, mStatusRequest: statusR, mSendAuthorizationDelegate: self)
        
        //consultar nuevamente search authorization:
        reload()
    }
    
    @objc func actionRechazar(sender: UIButton){
        let idR = idRequestA[sender.tag]
        let idEmpl = idEmployerA[sender.tag]
        var statusR = "4"
        statusR = Security.crypt(text: statusR)
        //servicio send authorization:
        mSendAuthorizationPresenter.getSendAuthorization(mIdRequested: idR, mIdEmployee: idEmpl, mStatusRequest: statusR, mSendAuthorizationDelegate: self)
        
        //consultar nuevamente search authorization:
        reload()
    }
    
    func reload() {
        //Query realm:
        var idEmpC: String?
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            //Servicio search autorization:
            //encriptar para consultar servicio:
            idEmpC = person.IdEmployee
            idEmpC = Security.crypt(text: idEmpC!)
            mAuthorizationPresenter.getSearchAuthorization(mIdEmployee: idEmpC!, mSearchAuthorizationDelegate: self)
        }
    }

}
