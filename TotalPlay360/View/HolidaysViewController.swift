//
//  HolidaysViewController.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 05/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift
import UICircularProgressRing

class HolidaysViewController: BaseViewController, InfoVacationsEmpDelegate, VacationStatusDelegate,  UIPickerViewDelegate, UIScrollViewDelegate {
    

    @IBOutlet weak var botonesView: UIView!
    
    @IBOutlet weak var SolicitudesScroll: UIScrollView!

    @IBOutlet weak var circularProgress1: UICircularProgressRingView!
    @IBOutlet weak var circularProgress2: UICircularProgressRingView!
    
    //View contornos:
    @IBOutlet weak var viewFechas: UIView!
    @IBOutlet weak var viewFechas2: UIView!
    @IBOutlet weak var txtFieldBorder: UITextField!
    
    @IBOutlet weak var countDays: UITextField!
    
    @IBOutlet weak var taken_label: UILabel!
    @IBOutlet weak var pendientesAuorizar_label: UILabel!
    @IBOutlet weak var porcentajeCircular1: UILabel!
    @IBOutlet weak var porcentajeCircular2: UILabel!
    
    //Realm:
    let realm = try! Realm()
    //Servicio get info vacations:
    var idEmpCrypt1: String?
    var mInfoVacationsEmpPresenter: InfoVacationsEmpPresenter!
    //servicio vacations status:
    var idEmpCrypt2: String?
    var mVacationsStatusPresenter: VacationStatusPresenter!
    
    
    //---------Date Picker-----------------------
    @IBOutlet weak var datePickerTxt1: UITextField!
    let datePicker1 = UIDatePicker()
    
    @IBOutlet weak var datePickerTxt2: UITextField!
    let datePicker2 = UIDatePicker()
    
    let colorM = UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0) //morado
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createDatePicker()
        LineNuevasSolicitudes.isHidden = true
        //loadVacaciones()
        
        //Para contornos view
        let colorBorder = UIColor(red: 107/255, green: 37/255, blue: 255/255, alpha: 1.0)
        viewFechas.layer.borderWidth = 1
        viewFechas.layer.borderColor = colorBorder.cgColor
        viewFechas2.layer.borderWidth = 1
        viewFechas2.layer.borderColor = colorBorder.cgColor
        
        txtFieldBorder.layer.borderWidth = 1
        txtFieldBorder.layer.borderColor = colorBorder.cgColor

        //Servicio:
        SolicitudesScroll.delegate = self
        SolicitudesScroll.isPagingEnabled = true
        
        //servicio get info vacations with realm:
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            
            
            //servicio info vacations:
             //encriptar para consultar:
             idEmpCrypt1 = person.IdEmployee
             idEmpCrypt1 = Security.crypt(text: idEmpCrypt1!)
            mInfoVacationsEmpPresenter.getInfoVacationsEmp(mIdEmployee: idEmpCrypt1!, mInfoVacationsEmpDelegate: self)
            
            //servicio vacations status:
             //encriptar para consultar:
             idEmpCrypt2 = person.IdEmployee
             idEmpCrypt2 = Security.crypt(text: idEmpCrypt2!)
            print("idEmpC: " + idEmpCrypt1!)
            mVacationsStatusPresenter.getVacationStatus(mIdEmployee: idEmpCrypt2!, mVacationStatusDelegate: self)
        }
        
    }
    
    
    func createDatePicker(){
        
        //format for picker
        datePicker1.datePickerMode = .date
        datePicker2.datePickerMode = .date
        
        //date spanish or other lenguaje
        let loc = Locale(identifier: "es")
        self.datePicker1.locale = loc
        self.datePicker2.locale = loc
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
       
        var components1 = DateComponents()
        var components2 = DateComponents()
        
        components1.day = +15
        
        
        //Agregar fecha maxima desde servicio:
        
        let minDate = Calendar.current.date(byAdding: components1, to: Date())
        //let maxxDate = Calendar.current.date(byAdding: components2, to: Date())
        datePicker1.minimumDate = minDate
        datePicker2.minimumDate = minDate
        
        datePicker1
        //datePicker1.maximumDate = maxxDate
        //datePicker2.maximumDate = maxxDate
        
        //bar button item 1
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        
        toolbar.setItems([doneButton], animated: false)
        
        datePickerTxt1.inputAccessoryView = toolbar
        datePickerTxt2.inputAccessoryView = toolbar
        
        //diseño de datepicker:
        toolbar.barTintColor = colorM
        doneButton.tintColor = UIColor.white
        datePicker1.backgroundColor = UIColor.white
        
        //assigning date picker to text field
        datePickerTxt1.inputView = datePicker1
        datePickerTxt2.inputView = datePicker2
    }

    @objc func donePressed(){
        
        //format date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        
        //date formater dd/mm/aaaa
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        datePickerTxt1.text = dateFormatter.string(from: datePicker1.date )
        datePickerTxt2.text = dateFormatter.string(from: datePicker2.date )
        self.view.endEditing(true)
        

        //Count days
        let startDate = dateFormatter.date(from: datePickerTxt1.text!)!
        let endDate = dateFormatter.date(from: datePickerTxt2.text!)!
        
        let calendar = Calendar.current
        let day1 = calendar.component(.day, from: startDate)
        let day2 = calendar.component(.day, from: endDate)
        let daysBetween: Int?
        
        if startDate != endDate {
            daysBetween = day2 - day1
        } else {
            daysBetween = (day2 - day1) + 1
        }
        

        let calendar2 = Calendar.current
        let d1 = calendar2.component(.weekday, from: startDate)
        let d2 = calendar2.component(.weekday, from: endDate)
        let dB = (d2 - d1)
        
        //print(calendar.component(.weekday, from: startDate))
        if calendar.component(.weekday, from: startDate) == 7 || calendar.component(.weekday, from: startDate) == 1  ||  calendar.component(.weekday, from: endDate) == 7 || calendar.component(.weekday, from: endDate) == 1 {
            countDays.text = "0"
        } else {
            countDays.text = String(describing: (daysBetween! + dB))
        }
        //countDays.text = String(describing: (daysBetween! + dB))
 
    }
    //-------------------------------------------------------
 
    
    //Boton regresar
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //-----------------Custom Alert-----------------------
    
    @IBAction func show_alert_action(_ sender: UIButton) {
        //show alert
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(vc)
        self.view.addSubview(vc.view)
        
        //datos()
        vc.date1.text = datePickerTxt1.text
        vc.date2.text = datePickerTxt2.text
    }
    
    
    //----------------------------------------------------
   
    
    
    //--------------Boton Mis solicitudes------------------
    @IBOutlet weak var MisSolitudesView: UIView!
    
    
    @IBOutlet weak var LineMisSolicitudes: UIView!
    
    @IBAction func solicitudes(_ sender: UIButton) {
        MisSolitudesView.isHidden = false
        NuevasSolicitudesView.isHidden = true
        LineMisSolicitudes.isHidden = false
        LineNuevasSolicitudes.isHidden = true
        
    }
    
    //----------------------------------------------------
    
    
    //------------Boton Nuevas solicitudes----------------
    
    @IBOutlet weak var NuevasSolicitudesView: UIView!
    
    
    @IBOutlet weak var LineNuevasSolicitudes: UIView!
    
    @IBAction func nuevasSolicitudes(_ sender: UIButton) {
        MisSolitudesView.isHidden = true
        NuevasSolicitudesView.isHidden = false
        LineNuevasSolicitudes.isHidden = false
        LineMisSolicitudes.isHidden = true
        
    }
    
    //servicio get info vacations y vacations status response:
    override func getPresenters() -> [BasePresenter]? {
        mInfoVacationsEmpPresenter = InfoVacationsEmpPresenter(viewController: self)
        
        mVacationsStatusPresenter = VacationStatusPresenter(viewController: self)
        return [mInfoVacationsEmpPresenter, mVacationsStatusPresenter]
    }
    //OnSucces Info Vacations:
    func OnSuccessInfoVacations(infoVacationsEmpResponse: InfoVacationsEmpResponse) {

        //desencriptar para mostrar:
        var dTaken = infoVacationsEmpResponse.Taken!
            dTaken = Security.decrypt(text: dTaken)

        var dTotal = infoVacationsEmpResponse.Total!
            dTotal = Security.decrypt(text: dTotal)
        
        var dPending = infoVacationsEmpResponse.Pending!
            dPending = Security.decrypt(text: dPending)
        
        //Pasar a float
        var floatTaken = CGFloat(NumberFormatter().number(from: dTaken)!)
        var floatTotal = CGFloat(NumberFormatter().number(from: dTotal)!)
        var floatPending = CGFloat(NumberFormatter().number(from: dPending)!)
        var pendings = floatTotal - floatPending - floatTaken
        
        if floatTaken != 0.0 {
            circularProgress1.value = floatTaken
            circularProgress1.valueIndicator = "/" + dTotal
            circularProgress1.maxValue = floatTotal
            
        } else {
            taken_label.isHidden = false
            taken_label.text = "0/" + dTotal
        }
        
        circularProgress2.value = pendings
        circularProgress2.valueIndicator = "/" + dTotal
        circularProgress2.maxValue = floatTotal
        
        var porcentaje1 = (100 * floatTaken) / floatTotal
        var porcentaje2 = (100 * pendings) / floatTotal
        
        
        porcentajeCircular1.text = String(format: "%.0f", porcentaje1) + "%"
        porcentajeCircular2.text = String(format: "%.0f", porcentaje2) + "%"
        
        pendientesAuorizar_label.text = String(format: "%.0f", floatPending)
    }

    //Onsucces Vacations Status:
    func OnSuccessVacationStatus(vacationStatusResponse: VacationStatusResponse) {
        
        let count = vacationStatusResponse.MVacationsEmployee.count
        
        //Scroll:
        SolicitudesScroll.contentSize = CGSize(width:  self.view.bounds.size.width * CGFloat(count), height: NuevasSolicitudesView.bounds.size.height)
        SolicitudesScroll.showsHorizontalScrollIndicator = false

        for index in 0 ..< count {
            if let view = Bundle.main.loadNibNamed("NuevasSolicitudesCollectionReusableView", owner: self, options: nil)?.first as? NuevasSolicitudesCollectionReusableView {

                //desencriptar datos:
                var fechaI = (vacationStatusResponse.MVacationsEmployee[index].DateStart!)
                    fechaI = Security.decrypt(text: fechaI)
                var fechaF = (vacationStatusResponse.MVacationsEmployee[index].DateEnd!)
                    fechaF = Security.decrypt(text: fechaF)
                var autoriza = (vacationStatusResponse.MVacationsEmployee[index].NameBoss!)
                    autoriza = Security.decrypt(text: autoriza)
                var fechaA = (vacationStatusResponse.MVacationsEmployee[index].DateRequest!)
                    fechaA = Security.decrypt(text: fechaA)
                var status = (vacationStatusResponse.MVacationsEmployee[index].StatusVacations!)
                    status = Security.decrypt(text: status)
                
                //Mostrar datos:
                
                view.fechaInicio.text =  fechaI
                view.fechaFin.text = fechaF
                view.autorizador.text = autoriza
                view.fechaAutorizador.text = fechaA
                
                if status == "1" {
                    view.estatus.text = "Tomados"
                } else if status == "2" {
                    view.estatus.text = "Autorizados"
                } else if status == "3" {
                    view.estatus.text = "En proceso"
                } else if status == "4" {
                    view.estatus.text = "No autorizado"
                }
                
                
                //Scroll
                SolicitudesScroll.addSubview(view)
                //view.constraintViewHolidays.constant = MisSolitudesView.bounds.size.height
                view.frame.size.width = self.view.bounds.size.width
                view.frame.origin.x = CGFloat(index) * CGFloat(210)

            }
        }
    }
    
}

