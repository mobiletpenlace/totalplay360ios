//
//  CapHumanoViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 05/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CapHumanoViewController: UIViewController {

    
    @IBOutlet weak var viewMiCapacitacion: UIView!
    
    @IBOutlet weak var viewVacaciones: UIView!
    
    @IBOutlet weak var viewAsistencia: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Sombra view Mi Capacitación
        viewMiCapacitacion.layer.shadowColor = UIColor.gray.cgColor
        viewMiCapacitacion.layer.shadowOpacity = 0.2
        viewMiCapacitacion.layer.shadowOffset = CGSize (width: 2 , height: 0)
        
        //Somnra view Vacaciones
        viewVacaciones.layer.shadowColor = UIColor.gray.cgColor
        viewVacaciones.layer.shadowOpacity = 0.2
        viewVacaciones.layer.shadowOffset = CGSize (width: 2 , height: 0)
        
        //Sombra view Asistencia
        viewAsistencia.layer.shadowColor = UIColor.gray.cgColor
        viewAsistencia.layer.shadowOpacity = 0.2
        viewAsistencia.layer.shadowOffset = CGSize (width: 2 , height: 0)
    }
    
    @IBAction func boton_menu(_ sender: UIButton) {
        //activar menu
        
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "MenuLateralViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "MenuLateralViewController") as! MenuLateralViewController
        //self.present(settingVC, animated: false, completion: {})
        
        settingVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(settingVC)
        self.view.addSubview(settingVC.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
