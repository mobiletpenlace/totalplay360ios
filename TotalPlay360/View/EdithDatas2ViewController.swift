//
//  EdithDatas2ViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 28/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import RealmSwift
import Darwin

class EdithDatas2ViewController: BaseViewController {

    //Servicio:
    
    
    @IBOutlet weak var mImagePerfil: UIImageView!
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var mName_label: UILabel!
    //@IBOutlet weak var mSurnamePatental_label: UILabel!
    //@IBOutlet weak var mSurnameMaternal_label: UILabel!
    @IBOutlet weak var mArea_label: UILabel!
    @IBOutlet weak var mPosition_label: UILabel!
    @IBOutlet weak var mStreet_txt: UITextField!
    @IBOutlet weak var mExtNumber_txt: UITextField!
    @IBOutlet weak var mIntNumber_txt: UITextField!
    @IBOutlet weak var mColony_txt: UITextField!
    @IBOutlet weak var mDelegation_txt: UITextField!
    @IBOutlet weak var mCode_txt: UITextField!
    //Image warning:
    @IBOutlet weak var mImg_warning1: UIImageView!
    @IBOutlet weak var mImg_warning2: UIImageView!
    @IBOutlet weak var mImg_warning3: UIImageView!
    @IBOutlet weak var mImg_warning4: UIImageView!
    @IBOutlet weak var mImg_warning5: UIImageView!
    @IBOutlet weak var mImg_warning6: UIImageView!
    
    //Realm:
    let realm = try! Realm()
    //Servicio GetInfoEmp:
    var numEmployeer: String?
    var mConsultaEmpleadoPresenter: ConsultaEmpleadoPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Query Realm:
        let allData = realm.objects(InfoEmployee.self)
        
        //Show image / selfie:
        for person in allData {
            //Cargar selfie tomada:
            //mImagePerfil.isHidden = false
            mImagePerfil.layer.cornerRadius = mImagePerfil.frame.size.width / 2
            mImagePerfil.clipsToBounds = true
            // mImagePerfil.image = UIImage(data: person.userPhoto!)
            
            //Cargar datos:
            mName_label.text = person.Name
            //mSurnamePatental_label.text = person.surnamePatental
            //mSurnameMaternal_label.text = person.surnameMaternal
            
            //Agregar datos faltantes...
        }
        //print(allData)
        
    }
    
    
    @IBAction func boton_back(_ sender: UIButton) {
        //exit(0)
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    /* @IBAction func boton_camara(_ sender: UIButton) {
     let storyboard2: UIStoryboard = UIStoryboard(name: "BienvenidaViewController", bundle: nil)
     let viewC2 = storyboard2.instantiateViewController(withIdentifier: "BienvenidaViewController")as! BienvenidaViewController
     self.present(viewC2, animated: false, completion: {})
     } */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func mStreet_action(_ sender: Any) {
        mImg_warning1.isHidden = true
    }
    
    @IBAction func mExtNumber_action(_ sender: Any) {
        mImg_warning2.isHidden = true
    }
    
    @IBAction func mIntNumber_action(_ sender: Any) {
        mImg_warning3.isHidden = true
    }
    
    @IBAction func mColony_action(_ sender: Any) {
        mImg_warning4.isHidden = true
    }
    
    @IBAction func mDelegation_action(_ sender: Any) {
        mImg_warning5.isHidden = true
    }
    
    @IBAction func mCode_action(_ sender: Any) {
        mImg_warning6.isHidden = true
    }
    
    @IBAction func GuardarDatos(_ sender: Any) {
      /*  if mStreet_txt.text != "" && mExtNumber_txt.text != "" && mIntNumber_txt.text != "" && mColony_txt.text != "" && mDelegation_txt.text != "" && mCode_txt.text != "" {
            //Guardar datos:
            let allData = realm.objects(Human.self)
            for person in allData {
                //Corregir
                if mStreet_txt.text != person.mail || mExtNumber_txt.text != person.edifice || mIntNumber_txt.text != person.floor || mColony_txt.text != person.extenssion || mDelegation_txt.text != person.phone || mCode_txt.text != person.phone {
                    //save new datas:
                    try! realm.write {
                        person.mail = mStreet_txt.text!
                        person.edifice = mExtNumber_txt.text!
                        person.floor = mIntNumber_txt.text!
                        person.extenssion = mColony_txt.text!
                        person.phone = mDelegation_txt.text!
                        person.phone = mCode_txt.text!
                    }
                    //print(allData)
                    //Encriptar antes de consultar sevicio:
                    var nEmpC = person.IdEmployee
                    nEmpC = Security.crypt(text: nEmpC)
                    var streetC = mStreet_txt.text!
                    streetC = Security.crypt(text: streetC)
                    var extNumberC = mExtNumber_txt.text!
                    extNumberC = Security.crypt(text: extNumberC)
                    var intNumberC = mIntNumber_txt.text!
                    intNumberC = Security.crypt(text: intNumberC)
                    var colonyC = mColony_txt.text!
                    colonyC = Security.crypt(text: colonyC)
                    var delegationC = mDelegation_txt.text!
                    delegationC = Security.crypt(text: delegationC)
                    
                    //sevicio update info emp:
                    //mModificarEmpleadoPresenter.getModificarEmpleado(mIdEmployee: nEmpC, mMail: streetC, mEdifice: edificeC, mFloor: floorC, mPhone: phoneC, mExtension: extencionC, mPictureURL: "", mModificarEmpleadoDelegate: self)
                    
                    let storyboard2: UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
                    let viewC2 = storyboard2.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
                    self.present(viewC2, animated: false, completion: {})
                } else {
                    let storyboard2: UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
                    let viewC2 = storyboard2.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
                    self.present(viewC2, animated: false, completion: {})
                }
            }
            
        } else {
            if mStreet_txt.text == "" || mExtNumber_txt.text == "" || mIntNumber_txt.text == "" || mColony_txt.text == "" || mDelegation_txt.text == "" || mCode_txt.text == "" {
                
                if mStreet_txt.text == "" {
                    mImg_warning1.isHidden = false
                    if mIntNumber_txt.text == "" {
                        mImg_warning2.isHidden = false
                        if mExtNumber_txt.text == "" {
                            mImg_warning3.isHidden = false
                            if mColony_txt.text == "" {
                                mImg_warning4.isHidden = false
                                if mDelegation_txt.text == "" {
                                    mImg_warning5.isHidden = false
                                    if mCode_txt.text == "" {
                                        mImg_warning6.isHidden = false
                                    }
                                }
                            }
                        }
                    }
                } else if mExtNumber_txt.text == "" {
                    mImg_warning2.isHidden = false
                    if mIntNumber_txt.text == "" {
                        mImg_warning3.isHidden = false
                        if mColony_txt.text == "" {
                            mImg_warning4.isHidden = false
                            if mDelegation_txt.text == "" {
                                mImg_warning5.isHidden = false
                                if mCode_txt.text == "" {
                                    mImg_warning6.isHidden = false
                                }
                            }
                        }
                    }
                } else if mIntNumber_txt.text == "" {
                    mImg_warning3.isHidden = false
                    if mColony_txt.text == "" {
                        mImg_warning4.isHidden = false
                        if mDelegation_txt.text == "" {
                            mImg_warning5.isHidden = false
                            if mCode_txt.text == "" {
                                mImg_warning6.isHidden = false
                            }
                        }
                    }
                } else if mColony_txt.text == "" {
                    mImg_warning4.isHidden = false
                    if mDelegation_txt.text == "" {
                        mImg_warning5.isHidden = false
                        if mCode_txt.text == "" {
                            mImg_warning6.isHidden = false
                        }
                    }
                } else if mDelegation_txt.text == "" {
                    mImg_warning5.isHidden = false
                    if mCode_txt.text == "" {
                        mImg_warning6.isHidden = false
                    }
                }
                if mCode_txt.text == "" {
                    mImg_warning6.isHidden = false
                }
            } //if principal
        }
        */
    }
    //Servicio onSucces:
    @IBAction func cancelar_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }

}
