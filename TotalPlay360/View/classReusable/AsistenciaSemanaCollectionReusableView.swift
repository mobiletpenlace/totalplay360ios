//
//  AsistenciaSemanaECollectionReusableView.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 24/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class AsistenciaSemanaCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var viewE: UIView!
    //@IBOutlet weak var viewS: UIView!
    @IBOutlet weak var actionCircleE: UIButton!
    //@IBOutlet weak var actionCircleS: UIButton!
    @IBOutlet weak var widthViewE: NSLayoutConstraint!
    @IBOutlet weak var heightViewE: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
