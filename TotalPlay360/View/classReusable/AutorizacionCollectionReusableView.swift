//
//  AutorizacionCollectionReusableView.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 06/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class AutorizacionCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var ViewWhite: UIView!
    @IBOutlet weak var Solicitante: UILabel!
    @IBOutlet weak var FechaFin: UILabel!
    @IBOutlet weak var NumeroDias: UILabel!
    @IBOutlet weak var FechaInicio: UILabel!
    
    @IBOutlet weak var button_authorize: UIButton!
    
    @IBOutlet weak var button_deny: UIButton!
    
    var statusR: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        ViewWhite.layer.shadowColor = UIColor.gray.cgColor
        ViewWhite.layer.shadowOpacity = 0.5
        ViewWhite.layer.shadowOffset = CGSize(width: 5, height: 0)
        
    }
}
