//
//  NuevasSolicitudesCollectionReusableView.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 24/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class NuevasSolicitudesCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var fechaInicio: UILabel!
    @IBOutlet weak var fechaFin: UILabel!
    @IBOutlet weak var autorizador: UILabel!
    @IBOutlet weak var fechaAutorizador: UILabel!
    @IBOutlet weak var estatus: UILabel!
    
    @IBOutlet weak var imageHolidays: UIImageView!
    
    @IBOutlet weak var viewFechas: UIView!
    @IBOutlet weak var viewDatas: UIView!
    
    @IBOutlet weak var stackView: UIStackView!
    
    //@IBOutlet weak var constraintViewHolidays: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageHolidays.layer.cornerRadius = 10
        
        imageHolidays.clipsToBounds = true
        
        //viewFechas
        viewFechas.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        //view Datas:
        let rectShape = CAShapeLayer()
        rectShape.bounds = stackView.frame
        rectShape.position = stackView.center
        rectShape.path = UIBezierPath(roundedRect: stackView.bounds,    byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 10, height: 10)).cgPath
        stackView.layer.backgroundColor = UIColor.white.cgColor
        //shadow:
        stackView.layer.shadowColor = UIColor.gray.cgColor
        stackView.layer.shadowOffset = CGSize(width: 0, height: 0)
        stackView.layer.shadowOpacity = 0.8
        stackView.layer.mask = rectShape
    }
}
