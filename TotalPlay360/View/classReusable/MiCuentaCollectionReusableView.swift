//
//  MiCuentaCollectionReusableView.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 05/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class MiCuentaCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var ViewMiCuenta: UIView!
    
    @IBOutlet weak var ScrollMiCuenta: UIScrollView!
    
        
}
