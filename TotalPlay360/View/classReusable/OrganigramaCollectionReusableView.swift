//
//  OrganigramaCollectionReusableView.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 24/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class OrganigramaCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var photo: UIImageView!
    
    @IBOutlet weak var title: UILabel!
}
