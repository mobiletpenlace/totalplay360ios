//
//  NotificationTableViewCell.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 06/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var imageP: UIImageView!
    @IBOutlet weak var imageS: UIImageView!
    @IBOutlet weak var notification: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        imageP.layer.cornerRadius = imageP.frame.size.width / 2
        imageP.clipsToBounds = true
        // Configure the view for the selected state
    }

}
