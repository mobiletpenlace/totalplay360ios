//
//  OrganigramaCollectionReusableView.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 05/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class OrganigramaCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var photo: UIImageView!
    
    @IBOutlet weak var title: UILabel!
}
