//
//  HomeViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 07/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import RealmSwift

class HomeViewController: UIViewController {

    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var name_label: UILabel!
    
    //Realm:
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewInfo.layer.shadowColor = UIColor.gray.cgColor
        viewInfo.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewInfo.layer.shadowOpacity = 0.8
        
        //Query Realm:
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            name_label.text = "¡ Bienvenido " + (person.Name) + "!"
        }
    }

    @IBAction func boton_menu(_ sender: UIButton) {
        //activar menu
        
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "MenuLateralViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "MenuLateralViewController") as! MenuLateralViewController
        //self.present(settingVC, animated: false, completion: {})
        
        settingVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(settingVC)
        self.view.addSubview(settingVC.view)
    }
    
    @IBAction func boton_notificaciones(_ sender: UIButton) {
        let storyboard2: UIStoryboard = UIStoryboard(name: "ListaNotificacionesViewController", bundle: nil)
        let viewC2 = storyboard2.instantiateViewController(withIdentifier: "ListaNotificacionesViewController")as! ListaNotificacionesViewController
        self.present(viewC2, animated: false, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
