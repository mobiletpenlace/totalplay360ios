//
//  ProfileDatasViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 25/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ProfileDatasViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "sbPage1"),
                self.newVc(viewController: "sbPage2"),
                self.newVc(viewController: "sbPage3"),
                self.newVc(viewController: "sbPage4")]
    }()
    
    var pageControl = UIPageControl()
    var viewDatas = UIView()
    var imageBackground = UIImageView()
    var buttonBack = UIButton()
    var imageP = UIImageView()
    var nameLabel = UILabel()
    var areaLabel = UILabel()
    var dptoLabel = UILabel()
    var buttonEdit = UIButton()
    var titleLabel = UILabel()
    var indexPage: Int?
    
    let colorPageControl = UIColor(red: 107/255, green: 39/255, blue: 255/255, alpha: 1.0) //morado
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        
        self.delegate = self
        configurePageControl()
        configureViewDatas()
    }
    
    func configurePageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = colorPageControl
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = colorPageControl
        self.view.addSubview(pageControl)
        
    }
    
    func configureViewDatas(){
        //self.view.bounds.size.width
        viewDatas = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.minY, width: UIScreen.main.bounds.width, height: 320))
        viewDatas.backgroundColor = UIColor.white
        self.view.addSubview(viewDatas)
        
        //Image trapedio:
        imageBackground = UIImageView(frame: CGRect(x: 20, y: 50, width: UIScreen.main.bounds.width - 40, height: self.viewDatas.bounds.size.height))
        imageBackground.image = UIImage(named: "img_trapecio.png")
        self.view.addSubview(imageBackground)
        
        //Boton regresar:
        buttonBack = UIButton(frame: CGRect(x: 30, y: 60, width: 40, height: 40))
        buttonBack.setImage(UIImage(named: "img_back2.png"), for: .normal)
        
        let selectorBack = #selector(self.selectButton(_:))
        buttonBack.addTarget(self, action: selectorBack, for: .touchUpInside)
        self.view.addSubview(buttonBack)
        
        //Image perfil:
        imageP = UIImageView(frame: CGRect(x: (self.viewDatas.bounds.size.width / 2) - 40, y: 70, width: 80, height: 80))
        imageP.image = UIImage(named: "img_girl2.png") //sustituir po la guardada en Realm/Perfil
        self.view.addSubview(imageP)
        
        //Label nombre:
        nameLabel = UILabel(frame: CGRect(x: 20, y: 160, width: UIScreen.main.bounds.width - 40, height: 20))
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont(name: "System", size: 6)
        nameLabel.text = "MARIANA QUINTOS" // Cambiar por nombre de realm
        
        self.view.addSubview(nameLabel)
        
        //label área:
        areaLabel = UILabel(frame: CGRect(x: 20, y: 185, width: (UIScreen.main.bounds.width / 2) - 20, height: 20))
        areaLabel.textAlignment = .right
        areaLabel.font = UIFont(name: "System", size: 1)
        areaLabel.text = "Sistemas - " // Cambiar por area de realm
        
        self.view.addSubview(areaLabel)
        
        //label departamento:
        areaLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: 185, width: UIScreen.main.bounds.width / 2, height: 20))
        areaLabel.textAlignment = .left
        areaLabel.font = UIFont(name: "System", size: 1)
        areaLabel.text = " Requerimientos" // Cambiar por dpto de realm
        
        self.view.addSubview(areaLabel)
        
        //Boton Editar:
        buttonEdit = UIButton(frame: CGRect(x: self.viewDatas.bounds.size.width - 80, y: 290, width: 40, height: 40))
        buttonEdit.setImage(UIImage(named: "img_edith.png"), for: .normal) //cambiar imagen
        let selectorEdit = #selector(self.selectButton(_:))
        buttonEdit.addTarget(self, action: selectorEdit, for: .touchUpInside)
        self.view.addSubview(buttonEdit)
        
        //label titulo:
        titleLabel = UILabel(frame: CGRect(x: 20, y: 295, width: UIScreen.main.bounds.width - 40, height: 20))
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: "System", size: 1)
        titleLabel.text = ""
        
        self.view.addSubview(titleLabel)
        
    }
    @objc func selectButton(_ sender: UIButton) {
        if sender === buttonBack {
            let storyboardBack: UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
            let viewBack = storyboardBack.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
            self.present(viewBack, animated: false, completion: {})
        } else if sender === buttonEdit {
            if indexPage == 0 {
                let storyboardEdit1: UIStoryboard = UIStoryboard(name: "EdithDatas1ViewController", bundle: nil)
                let viewEdit1 = storyboardEdit1.instantiateViewController(withIdentifier: "EdithDatas1ViewController")as! EdithDatas1ViewController
                self.present(viewEdit1, animated: false, completion: {})
            } else if indexPage == 1 {
                let storyboardEdit2: UIStoryboard = UIStoryboard(name: "EdithDatas2ViewController", bundle: nil)
                let viewEdit2 = storyboardEdit2.instantiateViewController(withIdentifier: "EdithDatas2ViewController")as! EdithDatas2ViewController
                self.present(viewEdit2, animated: false, completion: {})
            } else if indexPage == 2 {
                let storyboardEdit3: UIStoryboard = UIStoryboard(name: "EdithDatas3ViewController", bundle: nil)
                let viewEdit3 = storyboardEdit3.instantiateViewController(withIdentifier: "EdithDatas3ViewController")as! EdithDatas3ViewController
                self.present(viewEdit3, animated: false, completion: {})
            } else if indexPage == 3 {
                let storyboardEdit4: UIStoryboard = UIStoryboard(name: "EdithDatas4ViewController", bundle: nil)
                let viewEdit4 = storyboardEdit4.instantiateViewController(withIdentifier: "EdithDatas4ViewController")as! EdithDatas4ViewController
                self.present(viewEdit4, animated: false, completion: {})
            }
            
            /*let storyboardEdit: UIStoryboard = UIStoryboard(name: "EdithDatas1ViewController", bundle: nil)
            let viewEdit = storyboardEdit.instantiateViewController(withIdentifier: "EdithDatas1ViewController")as! EdithDatas1ViewController
            self.present(viewEdit, animated: false, completion: {}) */
        }
    }
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "ProfileDatasViewController", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
    
        let previousIndex = viewControllerIndex - 1

        if viewControllerIndex == 0 {
            titleLabel.text = ""
            indexPage = 0
        } else if viewControllerIndex == 1 {
            titleLabel.text = "Datos personales"
            indexPage = 1
        } else if viewControllerIndex == 2 {
            titleLabel.text = "Datos personales"
            indexPage = 2
        } else if viewControllerIndex == 3 {
            titleLabel.text = "Datos familiares"
            indexPage = 3
        }
        
        guard previousIndex >= 0 else {
            //return orderedViewControllers.last
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
       
        let nextIndex = viewControllerIndex + 1
        
        if viewControllerIndex == 0 {
            titleLabel.text = ""
            indexPage = 0
        } else if viewControllerIndex == 1 {
            titleLabel.text = "Datos personales"
            indexPage = 1
        } else if viewControllerIndex == 2 {
            titleLabel.text = "Datos personales"
            indexPage = 2
        } else if viewControllerIndex == 3 {
            titleLabel.text = "Datos familiares"
            indexPage = 3
        }
        
        guard orderedViewControllers.count != nextIndex else {
            //return orderedViewControllers.first
            return nil
        }
        
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
