//
//  Menu3ViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 27/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class Menu3ViewController: UIViewController {
    
    @IBOutlet weak var viewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var sideView: UIView!
    
    @IBOutlet weak var imgPerfil: UIImageView!
    
    @IBOutlet weak var botonR: UIButton!
    
    @IBOutlet weak var submenuView: UIView!
    
    
    @IBOutlet weak var submenu1: UILabel!
    
    @IBOutlet weak var submenu2: UILabel!
    
    @IBOutlet weak var submenu3: UILabel!
    
    @IBOutlet weak var submenu4: UILabel!
    
    @IBOutlet weak var submenu5: UILabel!
    
    @IBOutlet weak var submenu6: UILabel!
    
    @IBOutlet weak var submenuConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //blurView.layer.cornerRadius = 15
        sideView.layer.shadowColor = UIColor.gray.cgColor
        sideView.layer.shadowOpacity = 0.5
        sideView.layer.shadowOffset = CGSize (width: 5, height: 0)
        
        viewConstraint.constant = -280
        
        //image
        let myColor = UIColor(red: 108/255, green: 35/255, blue: 251/255, alpha: 1.0).cgColor
        imgPerfil.layer.borderWidth = 3
        imgPerfil.layer.borderColor = myColor
        imgPerfil.layer.cornerRadius = imgPerfil.frame.size.width/2
        imgPerfil.clipsToBounds = true
        
        //boton regresar
        botonR.isHidden = true
        
    }

    @IBAction func panPerfomed(_ sender: UIPanGestureRecognizer) {
        
        if sender.state == .began || sender.state == .changed {
            let translation = sender.translation(in: self.view).x
            
            if translation > 0 { //swipe right
                
                if viewConstraint.constant < 20 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.viewConstraint.constant += translation / 10
                        self.view.layoutIfNeeded()
                    })
                }
                
            }else { //swipe left
                if viewConstraint.constant > -280 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.viewConstraint.constant += translation / 10
                        self.view.layoutIfNeeded()
                    })
                }
            }
            
        } else if sender.state == .ended {
            
            if viewConstraint.constant < -100{
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewConstraint.constant = -280
                    self.view.layoutIfNeeded()
                })
                botonR.isHidden = true //para ocultar boton
                submenuView.isHidden = true //para ocultar submenu al ocultar menu principal
                submenu3.isHidden = false
                
            }else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })

            }
            
        }
    }
    
  @IBAction func boton_menu(_ sender: Any) {
        sideView.isHidden = false
        viewConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func back(_ sender: Any) {
        //navigationController?.popViewController(animated: true)
        //dismiss(animated: true, completion: nil)
        botonR.isHidden = true
        submenuView.isHidden = true
    }
    
    @IBAction func home_action(_ sender: UIButton) {
        
    }
    
    @IBAction func miCuenta_action(_ sender: UIButton) {
        botonR.isHidden = false
        submenuView.isHidden = false
        submenuConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        submenu1.text = "Mi Perfil"
        submenu2.text = "Habilidades y Competencias"
        submenu3.text = "Comunicación Interna"
        submenu4.text = "Vacaciones"
        submenu5.text = "Asistencia"
        submenu6.isHidden = true
    }
    
    @IBAction func herramientas_action(_ sender: UIButton) {
        botonR.isHidden = false
        submenuView.isHidden = false
        submenuConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        submenu1.text = "Proyectos"
        submenu2.text = "Viajes"
        submenu3.text = "Administrarme"
        submenu4.text = "Mis Activos"
        submenu5.text = "Mis Indicadores"
        submenu6.text = "Mis Compras"
    }
    
    
    @IBAction func capitalHumano_action(_ sender: UIButton) {
        botonR.isHidden = false
        submenuView.isHidden = false
        submenuConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        submenu1.text = "Mi Información"
        submenu2.text = "Mi plantilla"
        submenu3.text = "Cursos"
        submenu4.text = "Evaluaciones"
        submenu5.text = "Certificaciones"
        submenu6.text = "Plan Carrera"
    }
    
    
    @IBAction func objetivos_action(_ sender: UIButton) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
