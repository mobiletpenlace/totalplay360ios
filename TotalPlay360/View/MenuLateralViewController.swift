//
//  MenuLateralViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 06/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import RealmSwift

class MenuLateralViewController: UIViewController {
    //Realm:
    let realm = try! Realm()
    
    //Menu lateral
    @IBOutlet weak var viewConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var imgPerfil: UIImageView!
    @IBOutlet weak var botonR: UIButton?
    @IBOutlet weak var menuPrincipal: UILabel?
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet var viewP: UIView!

    @IBOutlet weak var submenuConstraint: NSLayoutConstraint?
    //-----------------------------------------------------
    let myColor = UIColor(red: 108/255, green: 35/255, blue: 251/255, alpha: 1.0).cgColor
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        //Query Realm data:
        let allData = realm.objects(InfoEmployee.self)
        for person in allData {
            //image
            
            imgPerfil.layer.borderWidth = 2
            imgPerfil.layer.borderColor = myColor
            imgPerfil.layer.cornerRadius = imgPerfil.frame.size.width/2
            imgPerfil.clipsToBounds = true
            //imgPerfil.image = UIImage(data: person.userPhoto!)
            nameLabel.text = person.Name
        }
        //Menu lateral
        //blurView.layer.cornerRadius = 15
        sideView.layer.shadowColor = UIColor.gray.cgColor
        sideView.layer.shadowOpacity = 0.5
        sideView.layer.shadowOffset = CGSize (width: 5, height: 0)
        
        viewConstraint.constant = -280
        
        //boton regresar
        botonR?.isHidden = true
        menuPrincipal?.isHidden = true
        
        //menu se muestra al dar clic en el moton de menu de c/pantalla
        sideView.isHidden = false
        viewConstraint.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        
    }

    //------------ Menu Lateral -------------------------
    
    //---------Pan gesture - Menú lateral ---------------
    
    @IBAction func panPerfomed(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let translation = sender.translation(in: self.view).x
            
            if translation > 0 { //swipe right
                
                if viewConstraint.constant < 20 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.viewConstraint.constant += translation / 10
                        self.view.layoutIfNeeded()
                    })
                }
                
            }else { //swipe left
                if viewConstraint.constant > -280 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.viewConstraint.constant += translation / 10
                        self.view.layoutIfNeeded()
                    })
                }
            }
            
        } else if sender.state == .ended {
            
            if viewConstraint.constant < -100{
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewConstraint.constant = -280
                    self.view.layoutIfNeeded()
                })
                botonR?.isHidden = true //para ocultar boton
                //submenuView.isHidden = true //para ocultar submenu al ocultar menu principal
                menuPrincipal?.isHidden = true
                //Para remover el menu lateral y que solo se muestre con el clic del moton de menu
                self.removeFromParentViewController()
                self.viewP.removeFromSuperview()
            }else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })
            }
            
        }
        
    }
    
    //--------menu lateral-------------------------
    
    @IBAction func menu_back(_ sender: UIButton) {
        self.removeFromParentViewController()
        self.viewP.removeFromSuperview()
    }
    
    @IBAction func back(_ sender: Any) {
        botonR?.isHidden = true
        menuPrincipal?.isHidden = true
        //Para ocultar submenu
        let vcS = childViewControllers.last as! SubmenuLateralViewController
        vcS.submenuView.isHidden = true
    }
    

    @IBAction func boton_Perfil(_ sender: UIButton) {
        let settingStoryboard : UIStoryboard = UIStoryboard(name: "CarruselViewController", bundle: nil)
        let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "CarruselViewController") as! CarruselViewController
        self.present(settingVC, animated: false, completion: {})
       
        //Para ocultar menu al cambiar de view controller
        self.removeFromParentViewController()
        self.viewP.removeFromSuperview()
    }
    
    //--------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
}
