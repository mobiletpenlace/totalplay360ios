//
//  ListaNotificacionesViewController.swift
//  TotalPlay360
//
//  Created by Marisol Huerta Ortega on 05/03/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ListaNotificacionesViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return arrIndexSection.count
    }
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrIndexSection[section]
    }

public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return iconoPrincipal.count
    }
    
public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationTableViewCell
    
    cell.imageP.image = UIImage(named: iconoPrincipal[indexPath.row])
    cell.imageS.image = UIImage(named: iconoSecundario[indexPath.row])
    cell.notification.text =  mensaje[indexPath.row]
    cell.time.text = fecha[indexPath.row]

    
    return (cell)
    
    }
    
    @IBOutlet weak var viewTable: UIView!
    
    let arrIndexSection = ["hoy","ayer","semana"]
    let iconoPrincipal = ["img_girl2.png" , "img_girl2.png", "img_girl3.png" , "img_boy3.png" , "img_boy2.png"]
    let iconoSecundario = ["img_comentario.png","img_revision.png","img_sugerencia.png", "img_revision.png","img_comentario.png"]
    let mensaje = ["In a storyboard-based application, you will often want", "Dispose of any resources that can be recreated." ,"Do any additional setup after loading the view", "Pass the selected object to the new view controller", "Do any additional setup after loading the view" ]
    let fecha = ["hace una hora", "hace dos horas", "lunes", "martes", "miercoles"]
    

  
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTable.layer.shadowColor = UIColor.gray.cgColor
        viewTable.layer.shadowOpacity = 0.5
        viewTable.layer.shadowOffset = CGSize(width: 5, height: 0)
        
        
    }

    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    

}
