//
//  AsistenciaViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 15/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit



class AsistenciaViewController: UIViewController {

    @IBOutlet weak var LabelHeader: UILabel!
    @IBOutlet weak var ViewCheckList: UIView!
    @IBOutlet weak var ViewShowList: UIView!
    @IBOutlet weak var SelectTime: UILabel!
    @IBOutlet weak var ViewMes: UIView!
    @IBOutlet weak var ViewSemana: UIView!
    @IBOutlet weak var Lunes: UILabel!
    @IBOutlet weak var Martes: UILabel!
    @IBOutlet weak var Miercoles: UIView!
    @IBOutlet weak var Jueves: UILabel!
    @IBOutlet weak var Viernes: UILabel!
    @IBOutlet weak var Sabado: UILabel!
    @IBOutlet weak var Domingo: UILabel!
    
    
    @IBOutlet weak var EntradaLunes: UIView!
    @IBOutlet weak var EntradaMartes: UIView!
    @IBOutlet weak var EntradaMiercoles: UIView!
    @IBOutlet weak var EntradaJueves: UIView!
    @IBOutlet weak var EntradaViernes: UIView!
    @IBOutlet weak var EntradaSabado: UIView!
    @IBOutlet weak var EntradaDomingo: UIView!
    
    @IBOutlet weak var SalidaLunes: UIView!
    @IBOutlet weak var SalidaMartes: UIView!
    @IBOutlet weak var SalidaMiercoles: UIView!
    @IBOutlet weak var SalidaJueves: UIView!
    @IBOutlet weak var SalidaViernes: UIView!
    @IBOutlet weak var SalidaSabado: UIView!
    @IBOutlet weak var SalidaDomingo: UIView!
    
    @IBOutlet weak var BarVerde: UIView!
    @IBOutlet weak var BarMorado: UIView!
    @IBOutlet weak var BarRojo: UIView!
    @IBOutlet weak var BarAmarillo: UIView!
    
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View3: UIView!
    @IBOutlet weak var View4: UIView!
    @IBOutlet weak var View5: UIView!
    @IBOutlet weak var View6: UIView!
    @IBOutlet weak var View7: UIView!
    @IBOutlet weak var View8: UIView!
    @IBOutlet weak var View9: UIView!
    @IBOutlet weak var View10: UIView!
    @IBOutlet weak var View11: UIView!
    @IBOutlet weak var View12: UIView!
    @IBOutlet weak var View13: UIView!
    @IBOutlet weak var View14: UIView!
    @IBOutlet weak var View15: UIView!
    @IBOutlet weak var View16: UIView!
    @IBOutlet weak var View17: UIView!
    @IBOutlet weak var View18: UIView!
    @IBOutlet weak var View19: UIView!
    @IBOutlet weak var View20: UIView!
    @IBOutlet weak var View21: UIView!
    @IBOutlet weak var View22: UIView!
    @IBOutlet weak var View23: UIView!
    @IBOutlet weak var View24: UIView!
    @IBOutlet weak var View25: UIView!
    @IBOutlet weak var View26: UIView!
    @IBOutlet weak var View27: UIView!
    @IBOutlet weak var View28: UIView!
    @IBOutlet weak var View29: UIView!
    @IBOutlet weak var View30: UIView!
    @IBOutlet weak var View31: UIView!
    @IBOutlet weak var View32: UIView!
    @IBOutlet weak var View33: UIView!
    @IBOutlet weak var View34: UIView!
    @IBOutlet weak var View35: UIView!
    @IBOutlet weak var View36: UIView!
    @IBOutlet weak var View37: UIView!
    
    @IBOutlet weak var Label1: UILabel!
    @IBOutlet weak var Label2: UILabel!
    @IBOutlet weak var Label3: UILabel!
    @IBOutlet weak var Label4: UILabel!
    @IBOutlet weak var Label5: UILabel!
    @IBOutlet weak var Label6: UILabel!
    @IBOutlet weak var Label7: UILabel!
    @IBOutlet weak var Label8: UILabel!
    @IBOutlet weak var Label9: UILabel!
    @IBOutlet weak var Label10: UILabel!
    @IBOutlet weak var Label11: UILabel!
    @IBOutlet weak var Label12: UILabel!
    @IBOutlet weak var Label13: UILabel!
    @IBOutlet weak var Label14: UILabel!
    @IBOutlet weak var Label15: UILabel!
    @IBOutlet weak var Label16: UILabel!
    @IBOutlet weak var Label17: UILabel!
    @IBOutlet weak var Label18: UILabel!
    @IBOutlet weak var Label19: UILabel!
    @IBOutlet weak var Label20: UILabel!
    @IBOutlet weak var Label21: UILabel!
    @IBOutlet weak var Label22: UILabel!
    @IBOutlet weak var Label23: UILabel!
    @IBOutlet weak var Label24: UILabel!
    @IBOutlet weak var Label25: UILabel!
    @IBOutlet weak var Label26: UILabel!
    @IBOutlet weak var Label27: UILabel!
    @IBOutlet weak var Label28: UILabel!
    @IBOutlet weak var Label29: UILabel!
    @IBOutlet weak var Label30: UILabel!
    @IBOutlet weak var Label31: UILabel!
    @IBOutlet weak var Label32: UILabel!
    @IBOutlet weak var Label33: UILabel!
    @IBOutlet weak var Label34: UILabel!
    @IBOutlet weak var Label35: UILabel!
    @IBOutlet weak var Label36: UILabel!
    @IBOutlet weak var Label37: UILabel!
    
    
     @IBOutlet weak var circle1: UIView!
     @IBOutlet weak var circle2: UIView!
     @IBOutlet weak var circle3: UIView!
     @IBOutlet weak var circle4: UIView!
     @IBOutlet weak var circle5: UIView!
     @IBOutlet weak var circle6: UIView!
     @IBOutlet weak var circle7: UIView!
     @IBOutlet weak var circle8: UIView!
     @IBOutlet weak var circle9: UIView!
     @IBOutlet weak var circle10: UIView!
     @IBOutlet weak var circle11: UIView!
     @IBOutlet weak var circle12: UIView!
     @IBOutlet weak var circle13: UIView!
     @IBOutlet weak var circle14: UIView!
     @IBOutlet weak var circle15: UIView!
     @IBOutlet weak var circle16: UIView!
     @IBOutlet weak var circle17: UIView!
     @IBOutlet weak var circle18: UIView!
     @IBOutlet weak var circle19: UIView!
     @IBOutlet weak var circle20: UIView!
     @IBOutlet weak var circle21: UIView!
     @IBOutlet weak var circle22: UIView!
     @IBOutlet weak var circle23: UIView!
     @IBOutlet weak var circle24: UIView!
     @IBOutlet weak var circle25: UIView!
     @IBOutlet weak var circle26: UIView!
     @IBOutlet weak var circle27: UIView!
     @IBOutlet weak var circle28: UIView!
     @IBOutlet weak var circle29: UIView!
     @IBOutlet weak var circle30: UIView!
     @IBOutlet weak var circle31: UIView!
     @IBOutlet weak var circle32: UIView!
     @IBOutlet weak var circle33: UIView!
     @IBOutlet weak var circle34: UIView!
     @IBOutlet weak var circle35: UIView!
     @IBOutlet weak var circle36: UIView!
     @IBOutlet weak var circle37: UIView!

    
    @IBOutlet weak var Bar1: UIView!

    @IBOutlet weak var Bar2: UIView!
    @IBOutlet weak var Bar3: UIView!
    @IBOutlet weak var Bar4: UIView!
    
    
    

    
    
    override func viewDidLoad() {
   ajustesSemana()
   ajustesMes()
        fecha()
   ViewSemana.isHidden = true
   ViewShowList.isHidden = true
   ViewCheckList.layer.borderWidth = 1
   ViewCheckList.layer.borderColor = (UIColor.black).cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ajustesSemana(){
        EntradaLunes.layer.cornerRadius = EntradaLunes.frame.size.width / 2
        EntradaLunes.clipsToBounds = true
        EntradaMartes.layer.cornerRadius = EntradaMartes.frame.size.width / 2
        EntradaMartes.clipsToBounds = true
        EntradaMiercoles.layer.cornerRadius = EntradaMiercoles.frame.size.width / 2
        EntradaMiercoles.clipsToBounds = true
        EntradaJueves.layer.cornerRadius = EntradaJueves.frame.size.width / 2
        EntradaJueves.clipsToBounds = true
        EntradaViernes.layer.cornerRadius = EntradaViernes.frame.size.width / 2
        EntradaViernes.clipsToBounds = true
        EntradaSabado.layer.cornerRadius = EntradaSabado.frame.size.width / 2
        EntradaSabado.clipsToBounds = true
        EntradaDomingo.layer.cornerRadius = EntradaDomingo.frame.size.width / 2
        EntradaDomingo.clipsToBounds = true
        
        SalidaLunes.layer.cornerRadius = SalidaLunes.frame.size.width / 2
        SalidaLunes.clipsToBounds = true
        SalidaMartes.layer.cornerRadius = SalidaMartes.frame.size.width / 2
        SalidaMartes.clipsToBounds = true
        SalidaMiercoles.layer.cornerRadius = SalidaMiercoles.frame.size.width / 2
        SalidaMiercoles.clipsToBounds = true
        SalidaJueves.layer.cornerRadius = SalidaJueves.frame.size.width / 2
        SalidaJueves.clipsToBounds = true
        SalidaViernes.layer.cornerRadius = SalidaViernes.frame.size.width / 2
        SalidaViernes.clipsToBounds = true
        SalidaSabado.layer.cornerRadius = SalidaSabado.frame.size.width / 2
        SalidaSabado.clipsToBounds = true
        SalidaDomingo.layer.cornerRadius = SalidaDomingo.frame.size.width / 2
        SalidaDomingo.clipsToBounds = true
        
        BarVerde.layer.cornerRadius = 4
        BarVerde.clipsToBounds = true
        BarAmarillo.layer.cornerRadius = 4
        BarAmarillo.clipsToBounds = true
        BarRojo.layer.cornerRadius = 4
        BarRojo.clipsToBounds = true
        BarMorado.layer.cornerRadius = 4
        BarMorado.clipsToBounds = true
        
        
        
    }
    
    func ajustesMes(){
        circle1.layer.cornerRadius = circle1.frame.size.width / 2
        circle1.clipsToBounds = true
        circle2.layer.cornerRadius = circle2.frame.size.width / 2
        circle2.clipsToBounds = true
        circle3.layer.cornerRadius = circle3.frame.size.width / 2
        circle3.clipsToBounds = true
        circle4.layer.cornerRadius = circle4.frame.size.width / 2
        circle4.clipsToBounds = true
        circle5.layer.cornerRadius = circle5.frame.size.width / 2
        circle5.clipsToBounds = true
        circle6.layer.cornerRadius = circle6.frame.size.width / 2
        circle6.clipsToBounds = true
        circle7.layer.cornerRadius = circle7.frame.size.width / 2
        circle7.clipsToBounds = true
        circle8.layer.cornerRadius = circle8.frame.size.width / 2
        circle8.clipsToBounds = true
        circle9.layer.cornerRadius = circle9.frame.size.width / 2
        circle9.clipsToBounds = true
        circle10.layer.cornerRadius = circle10.frame.size.width / 2
        circle10.clipsToBounds = true
        circle11.layer.cornerRadius = circle11.frame.size.width / 2
        circle11.clipsToBounds = true
        circle12.layer.cornerRadius = circle12.frame.size.width / 2
        circle12.clipsToBounds = true
        circle13.layer.cornerRadius = circle13.frame.size.width / 2
        circle13.clipsToBounds = true
        circle14.layer.cornerRadius = circle14.frame.size.width / 2
        circle14.clipsToBounds = true
        circle15.layer.cornerRadius = circle15.frame.size.width / 2
        circle15.clipsToBounds = true
        circle16.layer.cornerRadius = circle16.frame.size.width / 2
        circle16.clipsToBounds = true
        circle17.layer.cornerRadius = circle17.frame.size.width / 2
        circle17.clipsToBounds = true
        circle18.layer.cornerRadius = circle18.frame.size.width / 2
        circle18.clipsToBounds = true
        circle19.layer.cornerRadius = circle19.frame.size.width / 2
        circle19.clipsToBounds = true
        circle20.layer.cornerRadius = circle20.frame.size.width / 2
        circle20.clipsToBounds = true
        circle21.layer.cornerRadius = circle21.frame.size.width / 2
        circle21.clipsToBounds = true
        circle22.layer.cornerRadius = circle22.frame.size.width / 2
        circle22.clipsToBounds = true
        circle23.layer.cornerRadius = circle23.frame.size.width / 2
        circle23.clipsToBounds = true
        circle24.layer.cornerRadius = circle24.frame.size.width / 2
        circle24.clipsToBounds = true
        circle25.layer.cornerRadius = circle25.frame.size.width / 2
        circle25.clipsToBounds = true
        circle26.layer.cornerRadius = circle26.frame.size.width / 2
        circle26.clipsToBounds = true
        circle27.layer.cornerRadius = circle27.frame.size.width / 2
        circle27.clipsToBounds = true
        circle28.layer.cornerRadius = circle28.frame.size.width / 2
        circle28.clipsToBounds = true
        circle29.layer.cornerRadius = circle29.frame.size.width / 2
        circle29.clipsToBounds = true
        circle30.layer.cornerRadius = circle30.frame.size.width / 2
        circle30.clipsToBounds = true
        circle31.layer.cornerRadius = circle31.frame.size.width / 2
        circle31.clipsToBounds = true
        circle32.layer.cornerRadius = circle32.frame.size.width / 2
        circle32.clipsToBounds = true
        circle33.layer.cornerRadius = circle33.frame.size.width / 2
        circle33.clipsToBounds = true
        circle34.layer.cornerRadius = circle34.frame.size.width / 2
        circle34.clipsToBounds = true
        circle35.layer.cornerRadius = circle35.frame.size.width / 2
        circle35.clipsToBounds = true
        circle36.layer.cornerRadius = circle36.frame.size.width / 2
        circle36.clipsToBounds = true
        circle37.layer.cornerRadius = circle37.frame.size.width / 2
        circle37.clipsToBounds = true

        Bar1.layer.cornerRadius = 4
        Bar1.clipsToBounds = true
        Bar2.layer.cornerRadius = 4
        Bar2.clipsToBounds = true
        Bar3.layer.cornerRadius = 4
        Bar3.clipsToBounds = true
        Bar4.layer.cornerRadius = 4
        Bar4.clipsToBounds = true
        
        
        View32.layer.backgroundColor = (UIColor.white).cgColor
        View33.layer.backgroundColor = (UIColor.white).cgColor
        View34.layer.backgroundColor = (UIColor.white).cgColor
        View35.layer.backgroundColor = (UIColor.white).cgColor
        View36.layer.backgroundColor = (UIColor.white).cgColor
        View37.layer.backgroundColor = (UIColor.white).cgColor
        circle32.layer.backgroundColor = (UIColor.white).cgColor
        circle33.layer.backgroundColor = (UIColor.white).cgColor
        circle34.layer.backgroundColor = (UIColor.white).cgColor
        circle35.layer.backgroundColor = (UIColor.white).cgColor
        circle36.layer.backgroundColor = (UIColor.white).cgColor
        circle37.layer.backgroundColor = (UIColor.white).cgColor
        Label32.text = ""
        Label33.text = ""
        Label34.text = ""
        Label35.text = ""
        Label36.text = ""
        Label37.text = ""
        
    }
    
    @IBAction func SelectWeek(_ sender: Any) {
        SelectTime.text = "Semana"
        ViewMes.isHidden = true
        ViewSemana.isHidden = false
        ViewShowList.isHidden = true
    }
    
    @IBAction func SelectMonth(_ sender: Any) {
        SelectTime.text = "Mes"
        ViewShowList.isHidden = true
        ViewMes.isHidden = false
        ViewSemana.isHidden = true
        
    }
    
    @IBAction func ShowList(_ sender: Any) {
        ViewShowList.isHidden = false
        
    }
    
   
    
    func fecha(){
        
       //let date = Date()
       // let calendar = Calendar.current
       //  let weekOfyear = calendar.component(.weekOfYear, from: date)
       
  
        
    }
        
    @IBAction func botonMenu(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: false, completion: nil)
    }
    
    
  

}
