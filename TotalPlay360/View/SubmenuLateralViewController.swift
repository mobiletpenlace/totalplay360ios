//
//  SubmenuLateralViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 30/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SubmenuLateralViewController: UIViewController {

    //submenu
     //@IBOutlet weak var submenuConstraint: NSLayoutConstraint!
     @IBOutlet weak var submenuView: UIView!
     @IBOutlet weak var submenu1: UIButton!
     @IBOutlet weak var submenu2: UIButton!
     @IBOutlet weak var submenu3: UIButton!
     @IBOutlet weak var submenu4: UIButton!
     @IBOutlet weak var submenu5: UIButton!
     @IBOutlet weak var submenu6: UIButton!
     @IBOutlet weak var tituloMenu: UILabel!
    
    let myColor = UIColor(red: 108/255, green: 35/255, blue: 251/255, alpha: 1.0).cgColor
    
    @IBOutlet weak var comunTitle: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //boton comunicacion, bienestar y cultura
         comunTitle.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
         comunTitle.setTitle("Comunicación, bienestar \ny cultura", for: .normal)
    }
    
     @IBAction func home_actiom(_ sender: UIButton) {
     let settingStoryboard : UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
     let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
     self.present(settingVC, animated: false, completion: {})
     //Para ocultar menu al cambiar de view controller
     self.removeFromParentViewController()
     //self.viewP.removeFromSuperview()
     }
    
     @IBAction func miCuenta(_ sender: UIButton) {

        let vc = parent as! MenuLateralViewController
        vc.menuPrincipal?.isHidden = false
        vc.botonR?.isHidden = false
        vc.submenuConstraint?.constant = 0

        submenuView.isHidden = false
     UIView.animate(withDuration: 0.2, animations: {
     self.view.layoutIfNeeded()
     })
     tituloMenu.text = "Mi Cuenta"
     submenu1.setTitle("Vacaciones", for: .normal)
     submenu2.setTitle("Asistencia", for: .normal)
     submenu3.isHidden = true
     submenu4.isHidden = true
     submenu5.isHidden = true
     submenu6.isHidden = true
     
     //Para ocultar menu al cambiar de view controller
     
     }
     
     @IBAction func comunicacion_action(_ sender: UIButton) {
     
     let settingStoryboard : UIStoryboard = UIStoryboard(name: "ComunInterna1ViewController", bundle: nil)
     let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "ComunInterna1ViewController") as! ComunInterna1ViewController
     self.present(settingVC, animated: false, completion: {})
     
     //Para ocultar menu al cambiar de view controller
        let vc = parent as! MenuLateralViewController
        self.removeFromParentViewController()
        vc.viewP.removeFromSuperview()

     }
     
     @IBAction func autorizaciones_action(_ sender: UIButton) {
     let settingStoryboard : UIStoryboard = UIStoryboard(name: "ListaAutorizacionViewController", bundle: nil)
     let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "ListaAutorizacionViewController") as! ListaAutorizacionViewController
     self.present(settingVC, animated: false, completion: {})
     
        //Para ocultar menu al cambiar de view controller
        let vc = parent as! MenuLateralViewController
        self.removeFromParentViewController()
        vc.viewP.removeFromSuperview()
     }
     
     /*@IBAction func herramientas_action(_ sender: UIButton) {
     /*botonR.isHidden = false
     submenuView.isHidden = false
     submenuConstraint.constant = 0
     UIView.animate(withDuration: 0.2, animations: {
     self.view.layoutIfNeeded()
     })
     submenu1.setTitle("Proyectos", for: .normal)
     submenu2.setTitle("Viajes", for: .normal)
     submenu3.setTitle("Administrarme", for: .normal)
     submenu4.setTitle("Mis Activos", for: .normal)
     submenu5.setTitle("Mis Indicadores", for: .normal)
     submenu6.setTitle("Mis Compras", for: .normal)
     //submenu1.text = "Proyectos"
     //submenu2.text = "Viajes"
     //submenu3.text = "Administrarme"
     //submenu4.text = "Mis Activos"
     //submenu5.text = "Mis Indicadores"
     //submenu6.text = "Mis Compras" */
     } */
     
     @IBAction func logOut_action(_ sender: UIButton) {
     let viewAlert: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
     let viewAlertVC = viewAlert.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
     self.present(viewAlertVC, animated: false, completion: {})
        
     }
     @IBAction func capitalHumano_action(_ sender: UIButton) {
        //menuPrincipal.isHidden = false
        //botonR.isHidden = false
     submenuView.isHidden = false
     //submenuConstraint.constant = 0
     UIView.animate(withDuration: 0.2, animations: {
     self.view.layoutIfNeeded()
     })
     submenu1.setTitle("Mi Información", for: .normal)
     submenu2.setTitle("Mi plantilla", for: .normal)
     submenu3.setTitle("Cursos", for: .normal)
     submenu4.setTitle("Evaluaciones", for: .normal)
     submenu5.setTitle("Certificaciones", for: .normal)
     submenu6.setTitle("Plan Carrera", for: .normal)
     //submenu1.text = "Mi Información"
     //submenu2.text = "Mi plantilla"
     //submenu3.text = "Cursos"
     //submenu4.text = "Evaluaciones"
     //submenu5.text = "Certificaciones"
     //submenu6.text = "Plan Carrera"
     }
     
     @IBAction func objetivo_action(_ sender: UIButton) {
     
     }
     
     
     @IBAction func credencial_action(_ sender: UIButton) {
     let settingStoryboard : UIStoryboard = UIStoryboard(name: "CredencialElecViewController", bundle: nil)
     let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "CredencialElecViewController") as! CredencialElecViewController
     self.present(settingVC, animated: false, completion: {})
     
        //Para ocultar menu al cambiar de view controller
        let vc = parent as! MenuLateralViewController
        self.removeFromParentViewController()
        vc.viewP.removeFromSuperview()
        
     }
     
     @IBAction func boton1(_ sender: UIButton) {
     
     if submenu1.currentTitle == "Vacaciones" {
     let settingStoryboard : UIStoryboard = UIStoryboard(name: "HolidaysViewController", bundle: nil)
     let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "HolidaysViewController") as! HolidaysViewController
     self.present(settingVC, animated: false, completion: {})
     }
        //Para ocultar menu al cambiar de view controller
        let vc = parent as! MenuLateralViewController
        self.removeFromParentViewController()
        vc.viewP.removeFromSuperview()
     
     }
     @IBAction func boton2(_ sender: UIButton) {
     //Cambiar por pantalla correcta -->Asistencia-- a cuál de asistencia debe dirigirse?
     if submenu2.currentTitle == "Asistencia" {
     let settingStoryboard : UIStoryboard = UIStoryboard(name: "Asistencia2ViewController", bundle: nil)
     let settingVC = settingStoryboard.instantiateViewController(withIdentifier: "Asistencia2ViewController") as! Asistencia2ViewController
     self.present(settingVC, animated: false, completion: {})
     }
        //Para ocultar menu al cambiar de view controller
        let vc = parent as! MenuLateralViewController
        self.removeFromParentViewController()
        vc.viewP.removeFromSuperview()
     }
     
     @IBAction func boton3(_ sender: UIButton) {
     
     }
     
     @IBAction func boton4(_ sender: UIButton) {
     }
     
     @IBAction func boton5(_ sender: UIButton) {
     }
     
     @IBAction func boton6(_ sender: UIButton) {
     }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
