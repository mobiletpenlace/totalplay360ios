//
//  CustomAlertCameraViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 10/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CustomAlertCameraViewController: UIViewController{

    @IBOutlet weak var message_label: UILabel!
    @IBOutlet weak var action_Confirmar: UIButton!
    
    let color = UIColor(red: 96/255, green: 85/255, blue: 165/255, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set Label:
        let formatted = NSMutableAttributedString()
        formatted
        .bold("Toma la foto de frente:")
        .normal("Tu rostro deberá estar visible completamente.\n")
        .bold("\nCuida el fondo:")
        .normal("Te recomendamos utilizar uno blanco. \n")
        .bold("\nLa vestimenta es importante:")
        .normal("Recuerda que es una foto ejecutiva.")

        message_label.attributedText = formatted
    }
    
    @IBAction func confirmar_action(_ sender: UIButton) {
        //confirmar alert
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
        //Colo letra al pulsar
        action_Confirmar.titleLabel?.textColor = color 
        //show camera
       /* let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowCameraViewController") as! ShowCameraViewController
        
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(vc)
        self.view.addSubview(vc.view) */
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "AvenirNext-Medium", size: 12)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}
