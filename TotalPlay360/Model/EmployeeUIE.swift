//
//  EmployeeUIE.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class EmployeeUIE: NSObject, Mappable {
    required init?(map: Map) {
    }
    override init() {
    }
    
    func mapping(map: Map) {
        IdEmployee <- map["IdEmployee"]
        Mail <- map["Mail"]
        Edifice <- map["Edifice"]
        Floor <- map["Floor"]
        Phone <- map["Phone"]
        Extension <- map["Extension"]
        PictureURL <- map["PictureURL"]
    }
    var IdEmployee: String?
    var Mail: String?
    var Edifice: String?
    var Floor: String?
    var Phone: String?
    var Extension: String?
    var PictureURL: String?
}
