//
//  AllRealmObjects.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 07/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class InfoEmployee : Object{
    @objc dynamic var NumEmployee = ""
    @objc dynamic var userPhoto: Data?
    @objc dynamic var IdEmployee = ""
    @objc dynamic var IdBoss = ""
    @objc dynamic var NumEmployeeBoss = ""
    @objc dynamic var Area = ""
    @objc dynamic var Position = ""
    @objc dynamic var Name = ""
    @objc dynamic var surnameMaternal = ""
    @objc dynamic var surnamePatental = ""
    @objc dynamic var phone = ""
    @objc dynamic var extenssion = ""
    @objc dynamic var mail = ""
    @objc dynamic var edifice = ""
    @objc dynamic var floor = ""
    @objc dynamic var latitude = ""
    @objc dynamic var longitude = ""
}
