//
//  Result.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class Result: NSObject , Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        //enlazar con propiedades de objeto
        IdResult <- map["IdResult"]
        Result <- map["Result"]
        ResultDescription <- map["ResultDescription"]
    }
    
    var IdResult: String?
    var Result: String?
    var ResultDescription: String?

}
