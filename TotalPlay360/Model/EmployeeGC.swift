//
//  EmployeeGC.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class EmployeeGC: NSObject, Mappable {
    required init?(map: Map) {
    }
    override init() {
    }
    
    func mapping(map: Map) {
        Name <- map["Name"]
        SurnameMaternal <- map["SurnameMaternal"]
        SurnamePatental <- map["SurnamePatental"]
        PictureURL <- map["PictureURL"]
    }
    var Name: String?
    var SurnameMaternal: String?
    var SurnamePatental: String?
    var PictureURL: String?
}
