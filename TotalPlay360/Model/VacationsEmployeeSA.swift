//
//  VacationsEmployeeSA.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class VacationsEmployeeSA: NSObject, Mappable {
    required init?(map: Map) {
        
    }
    override init() {
        
    }
    func mapping(map: Map) {
        IdRequested <- map["IdRequested"]
        TypeRequest <- map["TypeRequest"]
        DateRequest <- map["DateRequest"]
        idEmployee <- map["idEmployee"]
        NameEmployee <- map["NameEmployee"]
        StartDate <- map["StartDate"]
        EndDate <- map["EndDate"]
    }
    var IdRequested: String?
    var TypeRequest: String?
    var DateRequest: String?
    var idEmployee: String?
    var NameEmployee: String?
    var StartDate: String?
    var EndDate: String?
}
