//
//  Employee.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 06/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class Employee: NSObject, Mappable {
    required init?(map: Map) {
        
    }
    override init() {
        
    }
    
    func mapping(map: Map) {
        //enlazar con propiedades de objeto
        IdEmployee <- map["IdEmployee"]
        IdBoss <- map["IdBoss"]
        NumEmployeeBoss <- map["NumEmployeeBoss"]
        Area <- map["Area"]
        Position <- map["Position"]
        Name <- map["Name"]
        SurnameMaternal <- map["SurnameMaternal"]
        SurnamePatental <- map["SurnamePatental"]
        PictureURL <- map["PictureURL"]
        Phone <- map["Phone"]
        Extension <- map["Extension"]
        Mail <- map["Mail"]
        Edifice <- map["Edifice"]
        Floor <- map["Floor"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
    }
    var IdEmployee: String?
    var IdBoss: String?
    var NumEmployeeBoss: String?
    var Area: String?
    var Position: String?
    var Name: String?
    var SurnameMaternal: String?
    var SurnamePatental: String?
    var PictureURL: String?
    var Phone: String?
    var Extension: String?
    var Mail: String?
    var Edifice: String?
    var Floor: String?
    var Latitude: String?
    var Longitude: String?
}

