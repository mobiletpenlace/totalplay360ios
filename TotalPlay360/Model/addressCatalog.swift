//
//  addressCatalog.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 22/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class addressCatalog: NSObject, Mappable {
    required init?(map: Map) {
    }
    override init() {
    }
    
    func mapping(map: Map) {
        codStatus <- map["codStatus"]
        desCodStatus <- map["desCodStatus"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
    var codStatus: String?
    var desCodStatus: String?
    var latitude: String?
    var longitude: String?
}
