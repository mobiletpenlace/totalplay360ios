//
//  VacationsEmployee.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class VacationsEmployee: NSObject, Mappable {
    required init?(map: Map) {
        
    }
    override init() {
        
    }
    func mapping(map: Map) {
        StatusVacations <- map["StatusVacations"]
        DateRequest <- map["DateRequest"]
        DateStart <- map["DateStart"]
        DateEnd <- map["DateEnd"]
        NameBoss <- map["NameBoss"]
    }
    var StatusVacations: String?
    var DateRequest: String?
    var DateStart: String?
    var DateEnd: String?
    var NameBoss: String?
    
}
