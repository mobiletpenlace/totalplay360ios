//
//  Login.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class Login: NSObject, Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        //enlazar con propiedades de objeto
        User <- map["User"]
        Password <- map["Password"]
        Ip <- map["Ip"]
    }
    override init() {
        
    }
    var User: String?
    var Password: String?
    var Ip: String?

}
