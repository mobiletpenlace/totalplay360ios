//
//  Day.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class Day: NSObject, Mappable {
    required init?(map: Map) {
        
    }
    override init() {
        
    }
    func mapping(map: Map) {
        Date <- map["Date"]
        StatusDay <- map["StatusDay"]
        TypeDay <- map["TypeDay"]
        Hour <- map["Hour"]
        
    }
    var Date: String?
    var StatusDay: String?
    var TypeDay: String?
    var Hour: String?
}
