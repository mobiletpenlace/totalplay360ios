//
//  ConsultaEmpleadoResponse.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 06/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class ConsultaEmpleadoResponse: BaseResponse {
    required init?(map: Map) {
        
    }
    override func mapping(map: Map) {
        MResult <- map["Result"]
        MEmployee <- map["Employee"]

    }
    var MResult: Result?
    var MEmployee: Employee?
}

