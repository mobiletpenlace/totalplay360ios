//
//  LoginResponse.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class LoginResponse: BaseResponse {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MResult <- map["Result"]
        userid <- map["userid"]
        name <- map["name"]
        Mail <- map["Mail"]
        NumEmp <- map["NumEmp"]
        Empresa <- map["Empresa"]
        Puesto <- map["Puesto"]
        FechaCad <- map["FechaCad"]
        CambioObliga <- map["CambioObliga"]
        TipoCuenta <- map["TipoCuenta"]
    }
    
    var MResult: Result?
    var userid: String?
    var name: String?
    var Mail: String?
    var NumEmp: String?
    var Empresa: String?
    var Puesto: String?
    var FechaCad: String?
    var CambioObliga: String?
    var TipoCuenta: String?
}
