//
//  VacationStatusResponse.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class VacationStatusResponse: BaseResponse {
    required init?(map: Map) {
        
    }
    override func mapping(map: Map) {
        MResult <- map["Result"]
        MVacationsEmployee <- map["VacationsEmployee"]
        
    }
    var MResult: Result?
    var MVacationsEmployee: [VacationsEmployee] = []
}
