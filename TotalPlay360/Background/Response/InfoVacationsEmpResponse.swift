//
//  InfoVacationsEmpResponse.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class InfoVacationsEmpResponse: BaseResponse {
    required init?(map: Map) {
        
    }
    override func mapping(map: Map) {
        MResult <- map["Result"]
        Total <- map["Total"]
        Taken <- map["Taken"]
        Pending <- map["Pending"]
        DateStart <- map["DateStart"]
        DateEnd <- map["DateEnd"]
    }
    var MResult: Result?
    var Total: String?
    var Taken: String?
    var Pending: String?
    var DateStart: String?
    var DateEnd: String?

}
