//
//  ConsultaColaboradoresResponse.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class ConsultaColaboradoresResponse: BaseResponse {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MResult <- map["Result"]
        MEmployeeGC <- map["Employee"]
        
    }
    var MResult: Result?
    var MEmployeeGC: [EmployeeGC] = []
}
