//
//  SearchAssistanceResponse.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class SearchAssistanceResponse: BaseResponse {
    required init?(map: Map) {
        
    }
    override func mapping(map: Map) {
        MResult <- map["Result"]
        MDay <- map["Day"]
    }
    var MResult: Result?
    var MDay: [Day] = []
}
