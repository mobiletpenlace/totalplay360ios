//
//  ApiDefinition.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ApiDefinition: NSObject {
    static let API_WS = "https://189.203.181.233:443/"
    
    static let WS_LOGIN = API_WS + "TotalPlay360/AuthenticationGS"
    static let WS_GETINFOEMP = API_WS + "TotalPlay360/GetInfoEmployee"
    static let WS_GETINFOCOLLAB = API_WS + "TotalPlay360/GetCollaborators"
    static let WS_UPDATEINFOEMP = API_WS + "TotalPlay360/UpdateInfoEmployee"
    static let WS_GETINFOVACATIONS = API_WS + "TotalPlay360/GetInfoVacationsEmployee"
    static let WS_VACATIONSTATUS = API_WS + "TotalPlay360/VacationsApplicationStatus"
    static let WS_SENDMAIL = API_WS + "TotalPlay/SendMail"
    static let WS_REQUESTVACATIONS = API_WS + "TotalPlay360/RequestVacations"
    static let WS_SEARCHAUTHORIZATION = API_WS + "TotalPlay360/SearchAuthorization"
    static let WS_SENDAUTHORIZATION = API_WS + "TotalPlay360/SendAuthorization"
    static let WS_REGISTERASSISTANCE = API_WS + "TotalPlay360/RegisterAssistance"
    static let WS_SEARCHASSISTANCE = API_WS + "TotalPlay360/SearchAssistance"
    static let WS_GETADDRESSC = API_WS + "TotalPlay360/GetAddressCatalog"
}
