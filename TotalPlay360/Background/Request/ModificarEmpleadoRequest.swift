//
//  ModificarEmpleadoRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class ModificarEmpleadoRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        MEmployeeUIE <- map["Employee"]
        
    }
    override init() {
        
    }
    var MLogin: Login?
    var MEmployeeUIE: EmployeeUIE?

}
