//
//  RegisterAssistanceRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class RegisterAssistanceRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        IdEmployee <- map["IdEmployee"]
        TypeAssistance <- map["TypeAssistance"]
    }
    override init() {
        
    }
    var MLogin: Login?
    var IdEmployee: String?
    var TypeAssistance: String?
}
