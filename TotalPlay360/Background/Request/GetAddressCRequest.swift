//
//  GetAddressCRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 22/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class GetAddressCRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    override func mapping(map: Map) {
        MLogin <- map["Login"]
    }
    override init() {
        
    }
    var MLogin: Login?
    
}
