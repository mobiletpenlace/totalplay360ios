//
//  SearchAssistanceRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class SearchAssistanceRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        IdEmployee <- map["IdEmployee"]
        StartDate <- map["StartDate"]
        EndDate <- map["EndDate"]
        Period <- map["Period"]
    }
    override init() {
        
    }
    var MLogin: Login?
    var IdEmployee: String?
    var StartDate: String?
    var EndDate: String?
    var Period: String?
}
