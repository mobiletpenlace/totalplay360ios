//
//  ConsultaEmpleadoRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 06/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class ConsultaEmpleadoRequest: BaseRequest{
    required init?(map: Map) {
        
    }
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        NumEmployee <- map["NumEmployee"]
    }
    override init() {
        
    }
    var MLogin: Login?
    var NumEmployee: String?
}
