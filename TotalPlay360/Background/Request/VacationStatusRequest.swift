//
//  VacationStatusRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class VacationStatusRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        IdEmployee <- map["IdEmployee"]
    }
    
    override init() {
        
    }
    var MLogin: Login?
    var IdEmployee: String?
}
