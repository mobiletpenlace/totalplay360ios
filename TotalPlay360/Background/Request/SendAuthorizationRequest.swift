//
//  SendAuthorizationRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class SendAuthorizationRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        IdRequested <- map["IdRequested"]
        IdEmployee <- map["IdEmployee"]
        StatusRequest <- map["StatusRequest"]
    }
    override init() {
        
    }
    var MLogin: Login?
    var IdRequested: String?
    var IdEmployee: String?
    var StatusRequest: String?
}
