//
//  SendMailRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 09/05/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class SendMailRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        to <- map["to"]
        cc <- map["cc"]
        bcc <- map["bcc"]
        reply_to <- map["reply_to"]
        subject <- map["subject"]
        body <- map["body"]
        from_Address <- map["from_Address"]
        from_Personal <- map["from_Personal"]
    }
    
    override init() {
        
    }
    var MLogin: Login?
    var to: String?
    var cc: String?
    var bcc: String?
    var reply_to: String?
    var subject: String?
    var body: String?
    var from_Address: String?
    var from_Personal: String?
}
