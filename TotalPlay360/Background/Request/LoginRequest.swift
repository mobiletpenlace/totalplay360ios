//
//  LoginRequest.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 04/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class LoginRequest: BaseRequest {
    required init?(map: Map) {
        
    }
    
    override func mapping(map: Map) {
        MLogin <- map["Login"]
        NumeroEmpleado <- map["NumeroEmpleado"]
        Llave <- map["Llave"]
    }
    override init() {
        
    }
    var MLogin: Login?
    var NumeroEmpleado: String?
    var Llave: String?

}
